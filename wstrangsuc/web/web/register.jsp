<%@page import="entities.User"%>
<%@page import="model.usermodel"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Thông tin cá nhân</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap styles -->
    <link href="assets/css/bootstrap.css" rel="stylesheet"/>
    <!-- Customize styles -->
    <link href="style.css" rel="stylesheet"/>
    <!-- font awesome styles -->
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet">
		<!--[if IE 7]>
			<link href="css/font-awesome-ie7.min.css" rel="stylesheet">
		<![endif]-->

		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

	<!-- Favicons -->
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
  </head>
<body>
	<!-- 
	Upper Header Section 
-->
<div class="container">
<%@include file="header.jsp" %>
<!-- End Lower Header Section -->

<!--
Navigation Bar Section 
-->
<%@include file="nav.jsp" %>
<!-- End Navigation Bar Section -->
<!-- 
Body Section 
-->
<%
    if(cookies !=null)
				{
				for(Cookie cookie : cookies)
				{
				    if(cookie.getName().equals("username")) 
				    	username = cookie.getValue();
				}
				}
    
                                usermodel user = new usermodel();
                                
                                
                                
%>
	<div class="row">

	<div class="span12">
    
          <% if(username==null) { %>
	<h3> Đăng ký</h3>
        <% } else {%>
        
        <h3>Sửa thông tin cá nhân </h3>
            
        <%  }%>
	<hr class="soft"/>
      
	<div class="well">
            <form class="form-horizontal" method="post" action="../ManagerUserServlet?command=<%if(username !=null) {out.print("update");} else {out.print("insert");}%>">
		<h3>Thông tin cá nhân</h3>
                <input type="hidden" name="id" value="<% if(username!=null) { User us = user.getUserName(username); out.print(us.getId()); } %>" >
                 <%
			if (username == null) {
                            
		%>
		<div class="control-group">
			<label class="control-label" for="inputFname" >Tên đăng nhập <sup>*</sup></label>
			<div class="controls">
                            <input type="text" id="usename" value="<%if(username!=null) { User us = user.getUserName(username); out.print(us.getUsername()); }%>" id="inputFname" required="không được bỏ trống" name="username" placeholder="user name">
			</div>
		 </div>
                        <%}%>
		 <div class="control-group">
			<label class="control-label" for="inputLname">Họ và tên</label>
			<div class="controls">
                            <input type="text" id="fullname" value="<%if(username!=null) { User us = user.getUserName(username); out.print(us.getFullname()); }%>" id="fullname" required="không được bỏ trống" name ="fullname" placeholder="full name">
			</div>
		 </div>
                 <%
			
			if (username == null) {
                            
		%>
                <div class="control-group">
                    <label class="control-label" for="inputLname"> Mật khẩu đăng nhập</label>
			<div class="controls">
                            <input type="password" id="password" required="không được bỏ trống" name ="password" placeholder="password">
			</div>
		 </div>
                <%}%>
                <div class="control-group">
			<label class="control-label" for="inputLname">Điện thoại</label>
			<div class="controls">
                            <input type="text" id="phone" value="<%if(username!=null) { User us = user.getUserName(username); out.print(us.getPhoneNumber()); }%>" required="không được bỏ trống" name ="phoneNumber" placeholder="phoneNumber">
			</div>
		 </div>
		<div class="control-group">
		<label class="control-label" for="inputEmail">Email <sup>*</sup></label>
		<div class="controls">
                    <input type="email" id="email" name="email" value="<%if(username!=null) { User us = user.getUserName(username); out.print(us.getEmail()); }%>" required="không được bỏ trống" placeholder="Email">
		</div>
                
	  </div>	  
		<div class="control-group">
			<label class="control-label" for="inputLname">Giới tính</label>
			<div class="controls">
                            <input type="radio"  name ="gender" value="0">Nam
                            <input type="radio"  name ="gender" value="1">Nữ
                            
			</div>
		 </div>
               
	<div class="control-group">
		<div class="controls">
                    <input type="submit" onclick="capnhat()" value="<% if(username==null) out.print("Đăng ký"); else out.print("Cập nhật"); %>" class="exclusive shopBtn">
		</div>
	</div>
	</form>
                <script>
                    function capnhat()
                    {
                        var username = document.getElementById("username").value;
                        var password = document.getElementById("password").value;
                        var fullname = document.getElementById("fullname").value;
                        var phone = document.getElementById("phone").value;
                        var email = document.getElementById("email").value;
                        if(username !="" && password !="" && fullname!="" && phone !="" && email!="")
                        {
                            alert("Đăng ký tài khoản thành công");
                            
                        }
                        else {
                            alert("Nhập đầy đủ thông tin");
                        }
                    }
                </script>
</div>
                    
</div>
</div>
<!-- 
Clients 
-->
<section class="our_client">
	<hr class="soften"/>
	<h4 class="title cntr"><span class="text">Manufactures</span></h4>
	<hr class="soften"/>
	<div class="row">
		<div class="span2">
			<a href="#"><img alt="" src="assets/img/1.png"></a>
		</div>
		<div class="span2">
			<a href="#"><img alt="" src="assets/img/2.png"></a>
		</div>
		<div class="span2">
			<a href="#"><img alt="" src="assets/img/3.png"></a>
		</div>
		<div class="span2">
			<a href="#"><img alt="" src="assets/img/4.png"></a>
		</div>
		<div class="span2">
			<a href="#"><img alt="" src="assets/img/5.png"></a>
		</div>
		<div class="span2">
			<a href="#"><img alt="" src="assets/img/6.png"></a>
		</div>
	</div>
</section>

<!--
Footer
-->
<footer class="footer">
<div class="row-fluid">
<div class="span2">
<h5>Your Account</h5>
<a href="#">YOUR ACCOUNT</a><br>
<a href="#">PERSONAL INFORMATION</a><br>
<a href="#">ADDRESSES</a><br>
<a href="#">DISCOUNT</a><br>
<a href="#">ORDER HISTORY</a><br>
 </div>
<div class="span2">
<h5>Iinformation</h5>
<a href="contact.html">CONTACT</a><br>
<a href="#">SITEMAP</a><br>
<a href="#">LEGAL NOTICE</a><br>
<a href="#">TERMS AND CONDITIONS</a><br>
<a href="#">ABOUT US</a><br>
 </div>
<div class="span2">
<h5>Our Offer</h5>
<a href="#">NEW PRODUCTS</a> <br>
<a href="#">TOP SELLERS</a><br>
<a href="#">SPECIALS</a><br>
<a href="#">MANUFACTURERS</a><br>
<a href="#">SUPPLIERS</a> <br/>
 </div>
 <div class="span6">
<h5>The standard chunk of Lorem</h5>
The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for
 those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et 
 Malorum" by Cicero are also reproduced in their exact original form, 
accompanied by English versions from the 1914 translation by H. Rackham.
 </div>
 </div>
</footer>
</div><!-- /container -->

<div class="copyright">
<div class="container">
	<p class="pull-right">
		<a href="#"><img src="assets/img/maestro.png" alt="payment"></a>
		<a href="#"><img src="assets/img/mc.png" alt="payment"></a>
		<a href="#"><img src="assets/img/pp.png" alt="payment"></a>
		<a href="#"><img src="assets/img/visa.png" alt="payment"></a>
		<a href="#"><img src="assets/img/disc.png" alt="payment"></a>
	</p>
	<span>Copyright &copy; 2013<br> bootstrap ecommerce shopping template</span>
</div>
</div>
<a href="#" class="gotop"><i class="icon-double-angle-up"></i></a>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.easing-1.3.min.js"></script>
    <script src="assets/js/jquery.scrollTo-1.4.3.1-min.js"></script>
    <script src="assets/js/shop.js"></script>
  </body>
</html>
