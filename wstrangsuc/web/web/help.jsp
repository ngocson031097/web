<%@page import="java.util.Map"%>
<%@page import="model.help"%>
<%@page import="model.helpmd"%>
<%@page import="entities.Help"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="model.helpmodel"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tư vấn thiết kế</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Bootstrap styles -->
        <link href="assets/css/bootstrap.css" rel="stylesheet"/>
        <!-- Customize styles -->
        <link href="style.css" rel="stylesheet"/>
        <!-- font awesome styles -->
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet"/>
        <style>
            div.stars {
                width: 270px;
                display: inline-block;
            }

            input.star { display: none; }

            label.star {
                float: right;
                padding: 10px;
                font-size: 36px;
                color: #444;
                transition: all .2s;
            }

            input.star:checked ~ label.star:before {
                content: '\f005';
                color: #FD4;
                transition: all .25s;
            }

            input.star-5:checked ~ label.star:before {
                color: #FE7;
                text-shadow: 0 0 20px #952;
            }

            input.star-1:checked ~ label.star:before { color: #F62; }

            label.star:hover { transform: rotate(-15deg) scale(1.3); }

            label.star:before {
                content: '\f006';
                font-family: FontAwesome;
            }
        </style>

        <!--[if IE 7]>
                <link href="css/font-awesome-ie7.min.css" rel="stylesheet">
        <![endif]-->

        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Favicons -->
        <link rel="shortcut icon" href="assets/ico/favicon.ico">
    </head>
    <body>
        <!-- 
        Upper Header Section 
        -->
        <%
             helpmd helpmd = (helpmd) session.getAttribute("sshelp");
               if(helpmd ==null)
               {
                   helpmd = new helpmd();
                   session.setAttribute("sshelp", helpmd);
               }
        %>
        <div class="container">
            <%@include file="header.jsp" %>
            <!-- End Lower Header Section -->
            <!--
            Navigation Bar Section 
            -->
            <jsp:include page="nav.jsp" />
            <!-- End Navigation Bar Section -->

            <section class="container">
                <div class="row" style="text-align: center;">


                    <h2>
                        Bạn cần tư vấn gì ?
                    </h2>
                    <form method="get" action="../ManagerhelpServlet">
                        <input type="text" name="ask" value="<% if (session.getAttribute("cauhoi") != null) {
                                out.print(session.getAttribute("cauhoi"));
                            } %>" placeholder="nhập câu hỏi bạn cần tư vấn"/>

                    </form>
                    <%

                        session.getAttribute("ans");
                        session.getAttribute("ask");
                        if (session.getAttribute("ans") != null) {

                    %>
                    <p><% out.print(session.getAttribute("ans"));
                        
                            
                    
                        
                        %></p>

                </div>
                 Vui lòng đánh giá câu trả lời
                <div class="row">
                    
                     <div class="stars">
                         <form method="post" action="../managerHelp?command=danhgia&id=<%=session.getAttribute("id")%>">
                            
                             <input class="star star-5" id="star-5" type="radio" onclick="check(this.value)" required name="star" value="5"/>
                            <label class="star star-5" for="star-5"></label>
                            <input class="star star-4" id="star-4" type="radio" onclick="check(this.value)" name="star" value="4"/>
                            <label class="star star-4" for="star-4"></label>
                            <input class="star star-3" id="star-3" type="radio" onclick="check(this.value)" name="star" value="3"/>
                            <label class="star star-3" for="star-3"></label>
                            <input class="star star-2" id="star-2" type="radio" onclick="check(this.value)" name="star" value="2"/>
                            <label class="star star-2" for="star-2"></label>
                            <input class="star star-1" id="star-1" type="radio" onclick="check(this.value)" name="star" value="1"/>
                            <label class="star star-1" for="star-1"></label>
                            <input type="text" id="danhgia" style="border:none; font-size: 20px; background:url(assets/img/white_leather.png) repeat 0 0; padding-left: 60px;">
                            
                            <input type="submit" value="Gửi đánh giá"/>
                            <script>
                                function check(star) {
                                    var kq;
                                    if(star==1)
                                        kq = "Rất kém";
                                    if(star==2)
                                        kq = "Kém";
                                    if(star==3)
                                        kq = "Khá";
                                    if(star==4)
                                        kq = "Tốt";
                                    if(star==5)
                                        
                                        kq = "Rất tốt";
                                    
                                    
                             document.getElementById("danhgia").value=kq;
                            }
                            </script>
                        </form>
                         
                    </div>
                        
                </div>
                    
                         <%}  if(session.getAttribute("cauhoi")!=null && session.getAttribute("ans")==null){%>
                 <p>Xin lỗi câu hỏi này chúng tôi chưa thể cập nhật.<br>
                     Theo bạn câu trả lời như thế nào là phù hợp.</p>
                    
                 <form method="post" action="../ManagerhelpServlet?comand=insert" >
                     <input type="hidden" name="cauhoi" value="<%=session.getAttribute("cauhoi")%>" >
                     <textarea id="answer" name="cautraloi"></textarea>
                     <br>
                     <input type="submit" onclick="ketqua()" value="Gửi đóng góp">
                 </form>
                 <%}%>
                 
                 <script>
                     function ketqua(){
                         var ans = document.getElementById("answer").value;
                         if(ans!="")
                             alert("cảm ơn đã gửi đóng góp");   
                     }
                 </script>
                 
                  
                 
                       


                <div class="row">
                    
                </div>

            </section>
            <!--
            Footer
            -->
            <footer class="footer">
                <div class="row-fluid">
                    <div class="span2">
                        <h5>Your Account</h5>
                        <a href="#">YOUR ACCOUNT</a><br>
                        <a href="#">PERSONAL INFORMATION</a><br>
                        <a href="#">ADDRESSES</a><br>
                        <a href="#">DISCOUNT</a><br>
                        <a href="#">ORDER HISTORY</a><br>
                    </div>
                    <div class="span2">
                        <h5>Iinformation</h5>
                        <a href="contact.html">CONTACT</a><br>
                        <a href="#">SITEMAP</a><br>
                        <a href="#">LEGAL NOTICE</a><br>
                        <a href="#">TERMS AND CONDITIONS</a><br>
                        <a href="#">ABOUT US</a><br>
                    </div>
                    <div class="span2">
                        <h5>Our Offer</h5>
                        <a href="#">NEW PRODUCTS</a> <br>
                        <a href="#">TOP SELLERS</a><br>
                        <a href="#">SPECIALS</a><br>
                        <a href="#">MANUFACTURERS</a><br>
                        <a href="#">SUPPLIERS</a> <br/>
                    </div>
                    <div class="span6">
                        <h5>The standard chunk of Lorem</h5>
                        The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for
                        those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et 
                        Malorum" by Cicero are also reproduced in their exact original form, 
                        accompanied by English versions from the 1914 translation by H. Rackham.
                    </div>
                </div>
            </footer>
        </div><!-- /container -->

        <div class="copyright">
            <div class="container">
                <p class="pull-right">
                    <a href="#"><img src="assets/img/maestro.png" alt="payment"></a>
                    <a href="#"><img src="assets/img/mc.png" alt="payment"></a>
                    <a href="#"><img src="assets/img/pp.png" alt="payment"></a>
                    <a href="#"><img src="assets/img/visa.png" alt="payment"></a>
                    <a href="#"><img src="assets/img/disc.png" alt="payment"></a>
                </p>
                <span>Copyright &copy; 2013<br> bootstrap ecommerce shopping template</span>
            </div>
        </div>
        <a href="#" class="gotop"><i class="icon-double-angle-up"></i></a>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="assets/js/jquery.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.easing-1.3.min.js"></script>
        <script src="assets/js/jquery.scrollTo-1.4.3.1-min.js"></script>
        <script src="assets/js/shop.js"></script>
    </body>
</html>