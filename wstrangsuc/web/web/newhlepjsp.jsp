<%-- 
    Document   : newhlepjsp
    Created on : Jun 2, 2018, 11:42:29 PM
    Author     : sonbh
--%>

<%@page import="model.helpmd"%>
<%@page import="model.help"%>
<%@page import="java.util.Map"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table>
            <tr>
                <th>id</th>
                <th>câu hỏi</th>
                <th>câu tl</th>
                <th>số lần</th>
                <th>đánh giá</th>
                
            </tr>
            
            <% 
               helpmd helpmd = (helpmd) session.getAttribute("sshelp");
               if(helpmd ==null)
               {
                   helpmd = new helpmd();
                   session.setAttribute("sshelp", helpmd);
               }
                for(Map.Entry<Integer , help> list : helpmd.getHelpItem().entrySet()){ %>
                <tr>
                    <td><%=list.getValue().getHelp().getIdhelp() %></td>
                    <td><%=list.getValue().getHelp().getAsk()%></td>
                    <td><%=list.getValue().getHelp().getAnswer()%></td>
                    <td><%=list.getValue().getSolan() %></td>
                    <td><%=list.getValue().getTb() %></td>
                </tr>
                <% } %>
            
        </table>
    </body>
</html>
