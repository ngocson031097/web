<%@page import="model.Cart"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="model.productmodel" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Danh sách sản phẩm</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap styles -->
    <link href="assets/css/bootstrap.css" rel="stylesheet"/>
    <!-- Customize styles -->
    <link href="style.css" rel="stylesheet"/>
    <!-- font awesome styles -->
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet">
		<!--[if IE 7]>
			<link href="css/font-awesome-ie7.min.css" rel="stylesheet">
		<![endif]-->

		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

	<!-- Favicons -->
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
  </head>
<body>
    <div class="container">
<!-- 
	Upper Header Section 
-->
<jsp:include page="header.jsp" />

<!--
Navigation Bar Section 
-->

<jsp:include page="nav.jsp" />
<!-- 
Body Section 
-->
<jsp:include page="bodysection.jsp" />
            
	<div class="span9">
<!-- 
New Products
-->
	<div class="well well-small">
	<h3>Danh sách sẩn phẩm</h3>
	
	<div class="row-fluid">
		  <ul class="thumbnails">
                      <% Cart cart =(Cart) session.getAttribute("cart");
                        if(cart == null)
                        {
                            session.setAttribute("cart", cart);
                        }
                      %>
                      <% 
                          int i=0;
                          if(request.getParameter("catid")!=null)
                          {
                          int id = Integer.parseInt(request.getParameter("catid")) ;
                          productmodel prmd = new productmodel();
                         
                      %>
                     
                      <c:forEach var="prcat" items="<%=prmd.getProductByIdCat(id)%>">
                          <li class="span4">
			  <div class="thumbnail">
				<a href="product_details??id=${prcat.idProduct}" class="overlay"></a>
				<a class="zoomTool" href="product_details.jsp?id=${prcat.idProduct}" title="add to cart"><span class="icon-search"></span>Xem chi tiết</a>
				<a href="product_details.jsp?id=${prcat.idProduct}"><img src="assets/img/${prcat.image}" alt=""></a>
				<div class="caption cntr">
					<p>${prcat.productName}</p>
					<p><strong> ${prcat.price}</strong></p>
					<h4><a class="shopBtn" href="../CartServlet?comand=addcart&id=${prcat.idProduct}" title="add to cart">Thêm vào giỏ</a></h4>
					<div class="actionList">
						<a class="pull-left" href="#">Add to Wish List </a> 
						<a class="pull-left" href="#"> Add to Compare </a>
					</div> 
					<br class="clr">
				</div>
			  </div>
			</li>
                        <%
                                        i++;
                                        if(i%3==0)
                                        {
                                            out.print("</ul></div><ul class=\"thumbnails\"><div class=\"row-fluid\">");
                                        }
                                    
                                    %>
                       <script>
                     $(".shopBtn").on("click",function (){
                         return false;
                     });
                    </script>
                      </c:forEach>
                      <% } 
                      else if(request.getParameter("search")!=null){
                        
                        String ten = request.getParameter("search");
                        productmodel searchpro = new productmodel();

                            
                      %>
                      <c:forEach var="prcat" items="<%=searchpro.searchProduct(ten)%>">
                          <li class="span4">
			  <div class="thumbnail">
				<a href="product_details??id=${prcat.idProduct}" class="overlay"></a>
				<a class="zoomTool" href="product_details.jsp?id=${prcat.idProduct}" title="add to cart"><span class="icon-search"></span>Xem chi tiết</a>
				<a href="product_details.jsp?id=${prcat.idProduct}"><img src="assets/img/${prcat.image}" alt=""></a>
				<div class="caption cntr">
					<p>${prcat.productName}</p>
					<p><strong> ${prcat.price}</strong></p>
					<h4><a class="shopBtn" href="../CartServlet?comand=addcart&id=${prcat.idProduct}" title="add to cart">Thêm hàng vào giỏ</a></h4>
					<div class="actionList">
						<a class="pull-left" href="#">Add to Wish List </a> 
						<a class="pull-left" href="#"> Add to Compare </a>
					</div> 
					<br class="clr">
				</div>
			  </div>
			</li>
                        <%
                                        i++;
                                        if(i%3==0)
                                        {
                                            out.print("</ul></div><ul class=\"thumbnails\"><div class=\"row-fluid\">");
                                        }
                                    
                                    %>
                      </c:forEach>
                        <%}
                       
                            else if(request.getParameter("catid")==null && request.getParameter("search")==null){
                        
                       // String ten = request.getParameter("search");
                        productmodel product = new productmodel();
                        //  int i=0;


                      %>
                      <c:forEach var="pro" items="<%=product.getAllProduct()%>">
                          <li class="span4">
			  <div class="thumbnail">
				<a href="product_details??id=${pro.idProduct}" class="overlay"></a>
				<a class="zoomTool" href="product_details.jsp?id=${pro.idProduct}" title="add to cart"><span class="icon-search"></span>Xem chi tiết</a>
				<a href="product_details.jsp?id=${pro.idProduct}"><img src="assets/img/${pro.image}" alt=""></a>
				<div class="caption cntr">
					<p>${pro.productName}</p>
					<p><strong> ${pro.price}</strong></p>
					<h4><a class="shopBtn" href="../CartServlet?comand=addcart&id=${pro.idProduct}" title="add to cart">Thêm hàng vào giỏ</a></h4>
					<div class="actionList">
						<a class="pull-left" href="#">Add to Wish List </a> 
						<a class="pull-left" href="#"> Add to Compare </a>
					</div> 
					<br class="clr">
				</div>
			  </div>
			</li>
                         <%
                                        i++;
                                        if(i%3==0)
                                        {
                                            out.print("</ul></div><ul class=\"thumbnails\"><div class=\"row-fluid\">");
                                        }
                                    
                                    %>
                      </c:forEach>
                        <%}%>
		  </ul>
		</div>
	
	</div>
</div>
<!-- 
Clients 
-->
<section class="our_client">
	<hr class="soften"/>
	<h4 class="title cntr"><span class="text">Manufactures</span></h4>
	<hr class="soften"/>
	<div class="row">
		<div class="span2">
			<a href="#"><img alt="" src="assets/img/1.png"></a>
		</div>
		<div class="span2">
			<a href="#"><img alt="" src="assets/img/2.png"></a>
		</div>
		<div class="span2">
			<a href="#"><img alt="" src="assets/img/3.png"></a>
		</div>
		<div class="span2">
			<a href="#"><img alt="" src="assets/img/4.png"></a>
		</div>
		<div class="span2">
			<a href="#"><img alt="" src="assets/img/5.png"></a>
		</div>
		<div class="span2">
			<a href="#"><img alt="" src="assets/img/6.png"></a>
		</div>
	</div>
</section>
    </div>
<!--
Footer
-->
<footer class="footer">
<div class="row-fluid">
<div class="span2">
<h5>Your Account</h5>
<a href="#">YOUR ACCOUNT</a><br>
<a href="#">PERSONAL INFORMATION</a><br>
<a href="#">ADDRESSES</a><br>
<a href="#">DISCOUNT</a><br>
<a href="#">ORDER HISTORY</a><br>
 </div>
<div class="span2">
<h5>Iinformation</h5>
<a href="contact.html">CONTACT</a><br>
<a href="#">SITEMAP</a><br>
<a href="#">LEGAL NOTICE</a><br>
<a href="#">TERMS AND CONDITIONS</a><br>
<a href="#">ABOUT US</a><br>
 </div>
<div class="span2">
<h5>Our Offer</h5>
<a href="#">NEW PRODUCTS</a> <br>
<a href="#">TOP SELLERS</a><br>
<a href="#">SPECIALS</a><br>
<a href="#">MANUFACTURERS</a><br>
<a href="#">SUPPLIERS</a> <br/>
 </div>
 <div class="span6">
<h5>The standard chunk of Lorem</h5>
The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for
 those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et 
 Malorum" by Cicero are also reproduced in their exact original form, 
accompanied by English versions from the 1914 translation by H. Rackham.
 </div>
 </div>
</footer>
</div><!-- /container -->

<div class="copyright">
<div class="container">
	<p class="pull-right">
		<a href="#"><img src="assets/img/maestro.png" alt="payment"></a>
		<a href="#"><img src="assets/img/mc.png" alt="payment"></a>
		<a href="#"><img src="assets/img/pp.png" alt="payment"></a>
		<a href="#"><img src="assets/img/visa.png" alt="payment"></a>
		<a href="#"><img src="assets/img/disc.png" alt="payment"></a>
	</p>
	<span>Copyright &copy; 2013<br> bootstrap ecommerce shopping template</span>
</div>
</div>
<a href="#" class="gotop"><i class="icon-double-angle-up"></i></a>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.easing-1.3.min.js"></script>
    <script src="assets/js/jquery.scrollTo-1.4.3.1-min.js"></script>
    <script src="assets/js/shop.js"></script>
  </body>
</html>