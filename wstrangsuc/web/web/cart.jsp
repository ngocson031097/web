<%@page import="java.text.DecimalFormat"%>
<%@page import="model.Item"%>
<%@page import="java.util.Map"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Giỏ hàng</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Bootstrap styles -->
        <link href="assets/css/bootstrap.css" rel="stylesheet"/>
        <!-- Customize styles -->
        <link href="style.css" rel="stylesheet"/>
        <!-- font awesome styles -->
        <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet">
        <!--[if IE 7]>
                <link href="css/font-awesome-ie7.min.css" rel="stylesheet">
        <![endif]-->

        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Favicons -->
        <link rel="shortcut icon" href="assets/ico/favicon.ico">
    </head>
    <body>
        <!-- 
                Upper Header Section 
        -->
        <div class="container">
            <%@include file="header.jsp" %>

            <!--
            Navigation Bar Section 
            -->
            <%@include file="nav.jsp" %>
            <!-- 
            Body Section 
            -->
            <div class="row">
                <div class="span12">
                    <ul class="breadcrumb">
                        <li><a href="index.html">Home</a> <span class="divider">/</span></li>
                        <li class="active">Check Out</li>
                    </ul>
                    <div class="well well-small">
                        
                        <h1>Giỏ hàng<small class="pull-right"></small></h1>
                        <hr class="soften"/>	

                        <table class="table table-bordered table-condensed">
                            <thead>
                                <tr>
                                    <th>Hỉnh ảnh Sản phẩm</th>
                                    <th>Chi tiết</th>
                                    <th>Giá</th>
                                    <th>Số lượng</th>
                                    <th>Tổng</th>
                                </tr>
                            </thead>
                            <tbody>
                                 <% DecimalFormat formatter = new DecimalFormat("###,###,###"); %>
                                <%for (Map.Entry<Integer, Item> list : cart.getCartsItem().entrySet()) {%>
                                <tr>
                                    <td><img width="200" src="assets/img/<%= list.getValue().getProduct().getImage()%>" alt=""></td>
                                    <td><b>Tên sản phẩm:</b><%= list.getValue().getProduct().getProductName()%><br>
                                        <b>Danh mục:</b><%= list.getValue().getProduct().getIdCate()%><br>
                                        <b>Mô tả:</b> <%= list.getValue().getProduct().getDescription()%></td>
                                    <td><% out.print(formatter.format(list.getValue().getProduct().getPrice())+"VNĐ");%></td>


                                    <td>
                                        <input class="span1" style="max-width:34px" placeholder="1" id="appendedInputButtons" size="16" type="text" value="<%= list.getValue().getQuantity()%>">
                                        <div class="input-append">
                                            <button class="btn btn-mini" type="button">-</button><button class="btn btn-mini" type="button"> + </button><a href="../CartServlet?comand=remove&id=<%=list.getValue().getProduct().getIdProduct() %>"><button class="btn btn-mini btn-danger" type="button" >X</button></a>
                                        </div>
                                    </td>
                                    <td><%out.print(formatter.format(list.getValue().getProduct().getPrice()*list.getValue().getQuantity())+"VNĐ"); %></td>
                                </tr>
                                <%}%>
                                <tr>
                                    <td class="alignR">Tổng tiền:	</td>
                                   

                                    <td><b><% out.print(formatter.format(cart.total())+ " VNĐ"); %></b>
                                   
                                        
                                    </td>
                                </tr>
                            </tbody>
                        </table><br/>		
                        <a href="index.jsp" class="shopBtn btn-large"><span class="icon-arrow-left"></span> Tiếp tục mua sản phẩm </a>
                        <a href="ckeckout.jsp" class="shopBtn btn-large pull-right">Đặt hàng<span class="icon-arrow-right"></span></a>

                    </div>
                </div>
            </div>
            <!-- 
            Clients 
            -->
            <section class="our_client">
                <hr class="soften"/>
                <h4 class="title cntr"><span class="text">Manufactures</span></h4>
                <hr class="soften"/>
                <div class="row">
                    <div class="span2">
                        <a href="#"><img alt="" src="assets/img/1.png"></a>
                    </div>
                    <div class="span2">
                        <a href="#"><img alt="" src="assets/img/2.png"></a>
                    </div>
                    <div class="span2">
                        <a href="#"><img alt="" src="assets/img/3.png"></a>
                    </div>
                    <div class="span2">
                        <a href="#"><img alt="" src="assets/img/4.png"></a>
                    </div>
                    <div class="span2">
                        <a href="#"><img alt="" src="assets/img/5.png"></a>
                    </div>
                    <div class="span2">
                        <a href="#"><img alt="" src="assets/img/6.png"></a>
                    </div>
                </div>
            </section>

            <!--
            Footer
            -->
            <footer class="footer">
                <div class="row-fluid">
                    <div class="span2">
                        <h5>Your Account</h5>
                        <a href="#">YOUR ACCOUNT</a><br>
                        <a href="#">PERSONAL INFORMATION</a><br>
                        <a href="#">ADDRESSES</a><br>
                        <a href="#">DISCOUNT</a><br>
                        <a href="#">ORDER HISTORY</a><br>
                    </div>
                    <div class="span2">
                        <h5>Iinformation</h5>
                        <a href="contact.html">CONTACT</a><br>
                        <a href="#">SITEMAP</a><br>
                        <a href="#">LEGAL NOTICE</a><br>
                        <a href="#">TERMS AND CONDITIONS</a><br>
                        <a href="#">ABOUT US</a><br>
                    </div>
                    <div class="span2">
                        <h5>Our Offer</h5>
                        <a href="#">NEW PRODUCTS</a> <br>
                        <a href="#">TOP SELLERS</a><br>
                        <a href="#">SPECIALS</a><br>
                        <a href="#">MANUFACTURERS</a><br>
                        <a href="#">SUPPLIERS</a> <br/>
                    </div>
                    <div class="span6">
                        <h5>The standard chunk of Lorem</h5>
                        The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for
                        those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et 
                        Malorum" by Cicero are also reproduced in their exact original form, 
                        accompanied by English versions from the 1914 translation by H. Rackham.
                    </div>
                </div>
            </footer>
        </div><!-- /container -->

        <div class="copyright">
            <div class="container">
                <p class="pull-right">
                    <a href="#"><img src="assets/img/maestro.png" alt="payment"></a>
                    <a href="#"><img src="assets/img/mc.png" alt="payment"></a>
                    <a href="#"><img src="assets/img/pp.png" alt="payment"></a>
                    <a href="#"><img src="assets/img/visa.png" alt="payment"></a>
                    <a href="#"><img src="assets/img/disc.png" alt="payment"></a>
                </p>
                <span>Copyright &copy; 2013<br> bootstrap ecommerce shopping template</span>
            </div>
        </div>
        <a href="#" class="gotop"><i class="icon-double-angle-up"></i></a>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="assets/js/jquery.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.easing-1.3.min.js"></script>
        <script src="assets/js/jquery.scrollTo-1.4.3.1-min.js"></script>
        <script src="assets/js/shop.js"></script>
    </body>
</html>
