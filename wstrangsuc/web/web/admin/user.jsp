<%@page import="entities.User"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="model.usermodel" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Quản lý tài khoản người dùng</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!--CUSTOM BASIC STYLES-->
    <link href="assets/css/basic.css" rel="stylesheet" />
    <!--CUSTOM MAIN STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <%@include file="header.jsp" %>
        <!-- /. NAV TOP  -->
        <jsp:include page="nav.jsp"/>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div class="col-md-12">
               <!--   Basic Table  -->
               <div class="panel panel-default">
                <div class="panel-heading">
                    Quản trị viên
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    
                                    <th>STT</th>
                                    <th style="width: 150px;">Tên người dùng</th>
                                    <th>Họ và tên</th>
                                    <th>email</th>
                                    <th>số điện thoại</th>
                                    <th>giới tính</th>
                                    <th>Người dùng</th>
                                    <th>Xử lý</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int i=1;
                                    usermodel usmd = new usermodel();
                                %>
                                <% for(User us :usmd.getAllUser()){ %>
                                <tr>
                                    <td><% out.print(i); %></td>
                                    <td><%=us.getUsername() %></td>
                                    <td><%=us.getFullname()%></td>
                                    <td><%=us.getEmail()%></td>
                                    <td><%=us.getPhoneNumber()%></td>
                                    <td><% if(us.getGender()==true ) out.print("Nam"); else out.print("Nữ");%></td>
                                    <td style="width: 100px;"><% if(Integer.parseInt(us.getPermission())==1 ) out.print("Quản trị viên"); else out.print("khách hàng"); %></td>
                                    <td><a href="../../ManagerUserServlet?id=<%= us.getId()%>&act=delete" onclick="return window.confirm('Bạn có chắc chắn muốn xóa?');">Xóa</a></td>
                                </tr>
                                <% i++;}%>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- End  Basic Table  -->
        </div>
    </div>
</div>
<!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->

<div id="footer-sec">
    &copy; 2014 YourCompany | Design By : <a href="http://www.binarytheme.com/" target="_blank">BinaryTheme.com</a>
</div>
<!-- /. FOOTER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="assets/js/bootstrap.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="assets/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="assets/js/custom.js"></script>
</body>
</html>


