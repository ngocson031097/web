<%@page import="java.text.DecimalFormat"%>
<%@page import="entities.TbOrder"%>
<%@page import="model.ordermodel"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Danh sách hóa đơn</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!--CUSTOM BASIC STYLES-->
    <link href="assets/css/basic.css" rel="stylesheet" />
    <!--CUSTOM MAIN STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <%@include file="header.jsp" %>
        <!-- /. NAV TOP  -->
        <jsp:include page="nav.jsp"/>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div class="col-md-12">
               <!--   Basic Table  -->
               <div class="panel panel-default">
                <div class="panel-heading">
                    Danh sách Hóa đơn
                </div>
                <div class="">
                    <div class="table-responsive">
                        <table class="table"  >
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Họ và tên</th>
                                    <th>Địa chỉ</th>
                                    <th>Điện thoại</th>
                                    <th>Email</th>
                                    <th>Tổng tiền</th>
                                    <th>Thời gian đặt</th>
                                     <th>Trạng Thái</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                 <% DecimalFormat formatter = new DecimalFormat("###,###,###"); %>
                                <% ordermodel order = new ordermodel();
                                int i = 1;
                                   for(TbOrder od : order.getAllOrder()){
                                %>
                                <tr >
                                    <td><% out.print(i); %></td>
                                    <td style="width: 180px;"><%= od.getCustomerFullName() %></td>
                                    <td style="width: 120px;"><%= od.getCustomerAddress()%></td>
                                    <td><%= od.getCustomerPhone()%></td>
                                    <td><%= od.getCustomerEmail()%></td>
                                    <td><% out.print(formatter.format(od.getTotalMoney()) +"VNĐ");%></td>
                                    <td><%= od.getCreatedAt()%></td>
                                    
                                    <td style="width: 120px;"><%if(od.getStatus()==0) out.print("<p style=\"color:red;\">chờ xác nhận</p>"); else out.print("<p style=\"color:green;\">Đã xác nhận</p>"); %></td>
                                    <td style="width: 120px;"><a href="orderdetail.jsp?id=<%= od.getIdOrder() %>">Chi tiết</a></td>
                                </tr>
                                <%i++;}%>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- End  Basic Table  -->
        </div>
    </div>
</div>
<!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->

<div id="footer-sec">
    &copy; 2014 YourCompany | Design By : <a href="http://www.binarytheme.com/" target="_blank">BinaryTheme.com</a>
</div>
<!-- /. FOOTER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="assets/js/bootstrap.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="assets/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="assets/js/custom.js"></script>
</body>
</html>


