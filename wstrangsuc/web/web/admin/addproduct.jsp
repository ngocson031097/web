<%@page import="entities.TbCategory"%>
<%@page import="model.catmodel"%>
<%@page import="entities.TbProduct"%>
<%@page import="model.productmodel"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Thêm sản phẩm mới</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!--CUSTOM BASIC STYLES-->
    <link href="assets/css/basic.css" rel="stylesheet" />
    <!--CUSTOM MAIN STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <%@include file="header.jsp" %>
        <!-- /. NAV TOP  -->
        <jsp:include page="nav.jsp"/>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div class="col-md-9">
               <!-- card -->
        <div class="card border-primary">
            <h2>Thêm sản phẩm</h2>
            <%
                
                if(request.getParameter("id")!=null)
                {
                    productmodel pr = new productmodel();
                    TbProduct product = new TbProduct();
                    product = pr.getproductid(Integer.parseInt(request.getParameter("id")));
                    System.out.println(request.getParameter("id"));
                }
                
               %>
               
               
               
            <div class="card-body">
                <!-- Nếu muốn upload được file, phải có thuộc tính enctype="multipart/form-data" -->
                <form method="post" action="../../ManagerProductServlet?command=insert">
                    <!-- form group -->
                   
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2 text-right">Tên sản phẩm</div>
                            <div class="col-md-10 ">
                                <input type="text" name="tensanpham"  id="" value=""required="không được bỏ trống" class="form-control" ></div>
                        </div>
                    </div>
                    <!-- end form group -->
                    <!-- form group -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2 text-right">Giá</div>
                            <div class="col-md-10 ">
                                
                                <input type="number" name="gia" value="" required class="form-control"></div>
                        </div>
                        <br />
                        <div class="form-group">
                        <div class="row">
                            <div class="col-md-2 text-right">Số lượng</div>
                            <div class="col-md-10 ">
                                
                                <input type="number" name="soluong" value="" required class="form-control"></div>
                        </div>
                    </div><!-- end form group -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2 text-right">Ảnh</div>
                            <div class="col-md-10 ">
                                <input type="file" name="anh" id="">
                            </div>
                        </div>
                    </div>
                    <!-- form group -->
                    
                    </div>
                    <!-- end form group -->
                    <!-- form group -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2 text-right">từ khóa</div>
                            <div class="col-md-10 "><input type="text" name="tukhoa" value="" class="form-control" / ></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2 text-right">mô tả</div>
                            <div class="col-md-10 "><textarea name="mota" style="width: 490px; border-radius: 3px; height:100px; "  ></textarea>
                            </div>
                        </div> 
                    </div>
                    <!-- end form group -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2 text-right">Trạng thái</div>
                            <div class="col-md-10 ">
                                <input type="checkbox" value="" name="trangthai" />
                        </div>
                    </div>
                        <div class="form-group">
                        <div class="row">
                            <div class="col-md-2 text-right">danh  mục</div>
                            <div class="col-md-10 ">
                                
                                <select name="danhmuc" required>
                                    <option value="0">--Lựa chọn danh mục--</option>
                                    <%
                                      catmodel cat = new catmodel();
                                     
                                    %>
                                    <c:forEach var="getcat" items="<%= cat.getAllCategory() %>">
                                        <option value="${getcat.idCate}">${getcat.cateName}</option>
                                    </c:forEach>
                                </select>
                        </div>
                    </div>
                    <!-- form group -->
                    
                    <!-- end form group -->
                    <!-- form group -->
                    
                    <!-- end form group -->
                    <!-- form group -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2 text-right"></div>
                            <div class="col-md-10 ">
                                <input type="submit" value="thêm" id="addnew"  class="btn btn-primary">
                                <input type="reset" value="Reset" class="btn btn-danger">
                            </div>
                        </div>
                    </div>
                    <!-- end form group -->
                    
                </form>
            </div>
        </div>
        </div>
    </div>
</div>
<!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->

<div id="footer-sec">
    &copy; 2014 YourCompany | Design By : <a href="http://www.binarytheme.com/" target="_blank">BinaryTheme.com</a>
</div>
<!-- /. FOOTER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="assets/js/bootstrap.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="assets/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="assets/js/custom.js"></script>
</body>
</html>


