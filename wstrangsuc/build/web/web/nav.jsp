<%-- 
    Document   : navbar
    Created on : Apr 1, 2018, 10:38:56 PM
    Author     : sonbh
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="model.catmodel" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="navbar">
	  <div class="navbar-inner">
		<div class="container">
		  <a data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </a>
		  <div class="nav-collapse">
			<ul class="nav">
			  <li class="active"><a href="index.jsp">Trang chủ</a></li>
                          <% 
                              catmodel prmd = new catmodel();
                              %>
                              <c:forEach var="pr" items="<%= prmd.getAllCategory()%>">
                              <li>
                                  <a href="products.jsp?catid=${pr.idCate}">${pr.cateName}</a>
                              </li>
                                
                              </c:forEach>
    			  <li class=""><a href="help.jsp">Tư vấn</a></li>
			  <li class=""><a href="contact.jsp">Liên hệ</a></li>
			</ul>
			<form action="products.jsp" class="navbar-search pull-left">
                            <input type="text" name="search" placeholder="TÌm kiếm" value="<%if(request.getParameter("search")!=null) out.print(request.getParameter("search")); else out.print("");%>" class="search-query span2">
                          
			</form>	
                              <%
			String username = null;
				Cookie[] cookies = request.getCookies();
				if(cookies !=null)
				{
				for(Cookie cookie : cookies)
				{
				    if(cookie.getName().equals("username")) 
				    	username = cookie.getValue();
				}
				}
                              
				
			if (username == null) {
                            
		%>
			<ul class="nav pull-right">
			<li class="dropdown">
				<a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="icon-lock"></span> Đăng nhập <b class="caret"></b></a>
				<div class="dropdown-menu">
                                    <form class="form-horizontal loginFrm" method="post" action="../LoginusServlet">
				  <div class="control-group">
					<input type="text" name="username" class="span2" id="inputEmail" placeholder="Tên đăng nhập">
				  </div>
				  <div class="control-group">
                                      <input type="password" name="password" class="span2" id="inputPassword" placeholder="Mật khẩu">
				  </div>
				  <div class="control-group">
					<label class="checkbox">
					<input type="checkbox"> Remember me
					</label>
					<button type="submit" class="shopBtn btn-block">Đăng nhập</button>
				  </div>
				</form>
				</div>
			</li>
			</ul>
                        <% } else { %>
                        <ul class="nav pull-right">
			<li class="dropdown">
				<a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="icon-lock"></span><%= username%><b class="caret"></b></a>
				<div class="dropdown-menu">
                                    
                                    <ul style="list-style: none; text-decoration: none; padding:5px;">
                                        <li style="padding-bottom:5px;">
                                            <a href="../LogoutusServlet" style="text-decoration: none;" >Đăng Xuất</a>
                                        </li>
                                        <li>
                                            <a href="register.jsp" style="text-decoration: none;">Sửa thông tin</a>
                                        </li>
                                    </ul>
				</div>
			</li>
			</ul>
                        <%} %>
		  </div>
		</div>
	  </div>
</div>
