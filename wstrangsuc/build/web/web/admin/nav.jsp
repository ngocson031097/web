<%-- 
    Document   : nav
    Created on : Apr 2, 2018, 1:33:13 AM
    Author     : sonbh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <div class="user-img-div">
                            <img src="assets/img/user.png" class="img-thumbnail" />

                            <div class="inner-text">
                                 <%
			String username = null;
				Cookie[] cookies = request.getCookies();
				if(cookies !=null)
				{
				for(Cookie cookie : cookies)
				{
				    if(cookie.getName().equals("username")) 
				    	username = cookie.getValue();
				}
				}
                              
				
			if (username == null) {
                            response.sendRedirect("login.jsp");
                        }
                        else out.print(username);
                            
		%>
                                <br />
                                <small>Last Login : 2 Weeks Ago </small>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a class="active-menu" href="https://dashboard.zopim.com/?first_login#home"><i class="fa fa-dashboard "></i>Dashboard</a>
                    </li>
                    <li>
                        <a href="#"></i> Quản lý sản phẩm <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="addproduct.jsp">Thêm sản phẩm</a></li>
                            <li><a href="listproduct.jsp">Danh sách sản phẩm</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="category.jsp"> Quản lý danh mục sản phẩm</a>
                     
                    </li>
                   
                    <li>
                        <a href="#"></i> Quản lý tài khoản người dùng <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="user.jsp">Danh sách người dùng</a></li>
                            <li><a href="addUser.jsp">Thêm tài khoản người dùng</a></li>
                            
                        </ul>
                    </li>
                    <li>
                        <a href="order.jsp">Danh sách hóa đơn</a>
                        
                    </li>
                     <!--  <li>
                        <a href="gallery.html"><i class="fa fa-anchor "></i>Gallery</a>
                    </li> -->
                </ul>
            </div>
        </nav>