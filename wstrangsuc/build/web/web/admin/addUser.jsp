<%@page import="entities.User"%>
<%@page import="model.usermodel"%>
<%@page import="entities.TbCategory"%>
<%@page import="model.catmodel"%>
<%@page import="entities.TbProduct"%>
<%@page import="model.productmodel"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Quản lý tài khoản người dùng</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!--CUSTOM BASIC STYLES-->
    <link href="assets/css/basic.css" rel="stylesheet" />
    <!--CUSTOM MAIN STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <%@include file="header.jsp" %>
        <!-- /. NAV TOP  -->
        <jsp:include page="nav.jsp"/>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div class="col-md-9">
               <!-- card -->
        <div class="card border-primary">
            <h2><%if(request.getParameter("id")!=null) out.print("Sửa thông tin tải khoản người dùng"); else out.print("Thêm tài khoản người dùng");%></h2>
            <div class="card-body">
                <!-- Nếu muốn upload được file, phải có thuộc tính enctype="multipart/form-data" -->
                <form method="post" action="../../ManagerUserServlet?act=<%if(request.getParameter("id")!=null) out.print("update"); else out.print("insert"); %>">
                    <!-- form group -->
                     <div class="form-group">
                        <div class="row">
                            <label class="col-md-3 text-right"></label>
                            <div class="col-md-9 ">
                                <input type="hidden" name="id" value="<%if(request.getParameter("id")!=null) out.print(request.getParameter("id")); %>"></div>
                        </div>
                    </div>
                        
                        <%
                            String id = request.getParameter("id");
                            //if(id!=null){
                                usermodel usmd = new usermodel();
                               // User user = usmd.getUserId(Integer.parseInt(id));
                                
                            
                        %>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-3 text-right">Tên đăng nhập</label>
                            <div class="col-md-9 ">
                                <input type="text" name="username"  id="" value="<%if(id!=null){User user = usmd.getUserId(Integer.parseInt(id)); out.print(user.getUsername());} ; %>" required="không được bỏ trống" class="form-control" ></div>
                        </div>
                    </div>
                    <!-- end form group -->
                    <!-- form group -->
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-3 text-right">Họ và tên</label>
                            <div class="col-md-9 ">
                                
                                <input type="text" name="fullname" value="<%if(id!=null){User user = usmd.getUserId(Integer.parseInt(id)); out.print(user.getFullname());} ; %>" required="không được bỏ trống" class="form-control"></div>
                        </div>
                        
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-3 text-right">Giới tính</label>
                            <div class="col-md-9">
                                <select  name="gender" >
                                   
                                    <option value="1">Nam</option>
                                    <option value="0">Nữ</option>
                                </select>
                            </div>
                        </div>
                        
                    </div>
                    
                        <br />
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-3 text-right ">Mật khẩu</label>
                            <div class="col-md-9 ">
                                
                                <input type="password" name="password" value="<%if(id!=null){User user = usmd.getUserId(Integer.parseInt(id)); out.print(user.getPassword());} ; %>" required="không được bỏ trống" class="form-control"></div>
                        </div>
                    </div><!-- end form group -->
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-3 text-right">Email</label>
                            <div class="col-md-9 ">
                                
                                <input type="email" name="email" value="<%if(id!=null){User user = usmd.getUserId(Integer.parseInt(id)); out.print(user.getEmail());} ; %>" required="không được bỏ trống" class="form-control"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-3 text-right">Số điện thoại</label>
                            <div class="col-md-9 ">
                                
                                <input type="text" name="phoneNumber" value="<%if(id!=null){User user = usmd.getUserId(Integer.parseInt(id)); out.print(user.getPhoneNumber());} ; %>" required="không được bỏ trống" class="form-control"></div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        
			<label class="col-md-3 text-right" for="inputLname">Loại người dùng</label>
                        <select  name="permission" >
                                    <option value="0">--Lựa chọ người dùng--</option>
                                    <option value="1">Quản trị viên</option>
                                    <option value="0">Khách hàng</option>
                        </select>
                        
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2 text-right"></div>
                            <div class="col-md-10 ">
                                <input type="submit" value="<%if(request.getParameter("id")!=null) out.print("Cập nhật"); else out.print("Thêm mới");%>" id="addnew"  class="btn btn-primary">
                                <input type="reset" value="Reset" class="btn btn-danger">
                            </div>
                        </div>
                    </div>
                    <!-- end form group -->
                    
                </form>
            </div>
        </div>
        </div>
    </div>
</div>
<!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->

<div id="footer-sec">
    &copy; 2014 YourCompany | Design By : <a href="http://www.binarytheme.com/" target="_blank">BinaryTheme.com</a>
</div>
<!-- /. FOOTER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="assets/js/bootstrap.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="assets/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="assets/js/custom.js"></script>
</body>
</html>


