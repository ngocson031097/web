 <%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="entities.TbCategory"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="model.productmodel" %>
<%@page import="model.catmodel" %>
<%@page import="entities.*" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Danh sách sản phẩm</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!--CUSTOM BASIC STYLES-->
    <link href="assets/css/basic.css" rel="stylesheet" />
    <!--CUSTOM MAIN STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <style>
        table tr td {
            border: solid 1px black;
        }
        table th td {
            border: solid 1px black;
        }
                                
     </style>
</head>
<body>
    
    <div id="wrapper">
        <%@include file="header.jsp" %>
        <!-- /. NAV TOP  -->
        <jsp:include page="nav.jsp"/>
        <!-- /. NAV SIDE  -->
        <section>
        <div id="page-wrapper">
            <div class="c">
               <!--   Basic Table  -->
               <div class="panel panel-default">
                <div class="panel-heading">
                   Sản phẩm
                   
                   <br>
                       <a href="addproduct.jsp?command=insert">Thêm mới</a>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        
                        <table class="table">
                            
                            <thead>
                                <tr>
                                    
                                    <th>STT</th>
                                    <th style="width: 100px">Tên sản phẩm</th>
                                    <th>giá</th>
                                    <th>Số lượng</th>
                                    <th>Ảnh</th>
                                    <th>Mô tả</th>
                                    <th>danh mục</th>
                                    <th>Thời gian tạo</th>
                                    <th>Thời gian cập nhật</th>
                                    <th>Xử lý</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int i=1;
                                
                                    productmodel prdmd = new productmodel();
                                    List<TbProduct> listpro = prdmd.getAllProduct();
                                   // catmodel cate = new catmodel();
                                    
                                    
                                %>
                                <% for (TbProduct product: listpro){ %>
                                <tr>
                                    <td><% out.print(i); %></td>
                                    <td style="width: 100px"><%= product.getProductName() %></td>
                                    <td><%= product.getPrice()%></td>
                                    <td><%= product.getQuantity() %></td>
                                    <td><img src="../assets/img/<%= product.getImage()%>" width="100px" height="100px"></td>
                                    <td><%= product.getDescription() %> </td>
                                    <td>
                                         <%
                                         //  catmodel cate = new catmodel();
                                          // TbCategory cat = cate.getcategoryByid(product.getIdCate());
                                           // out.print(cat.getCateName() );
                                        %>
                                        <td style="width: 100px;"><%=product.getCreatedAt()%></td>
                                        <td style="width: 100px;"><%= product.getUpdatedAt() %></td>
                                    
                                    </td>
                                        
                                        <td>
                                            <a href="updateproduct.jsp?command=edit&&id=<%= product.getIdProduct()%>">Sửa</a><br>
                                                <br>
                                                    <a href="../../ManagerProductServlet?command=delete&&id=<%= product.getIdProduct() %>" onclick="return window.confirm('Bạn có chắc chắn muốn xóa?  ')">Xóa</a>
                                        
                                        </td>
                                                    
                                   
                                </tr>
                                <% i++; }%>
                                
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- End  Basic Table  -->
        </div>
    </div>
 </section>
                  
</div>
<!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->
<!--<footer>
<div id="footer-sec">
    &copy; 2014 YourCompany | Design By : <a href="http://www.binarytheme.com/" target="_blank">BinaryTheme.com</a>
</div>
</footer>-->
<!-- /. FOOTER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="assets/js/bootstrap.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="assets/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="assets/js/custom.js"></script>
</body><script>
    function del()
    {
        alert("Xóa thành công");
    }
</script>
</html>


