<%@page import="entities.TbOrder"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="entities.TbOrderdetail"%>
<%@page import="model.ordermodel"%>
<%@page import="entities.User"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="model.usermodel" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Chi tiết hóa đơn</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!--CUSTOM BASIC STYLES-->
    <link href="assets/css/basic.css" rel="stylesheet" />
    <!--CUSTOM MAIN STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <%@include file="header.jsp" %>
        <!-- /. NAV TOP  -->
        <jsp:include page="nav.jsp"/>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            
            <div class="col-md-12">
                <h2>Chi Tiết hóa đơn</h2>
                <div>
                    <% ordermodel ormd = new ordermodel();
                       TbOrder orderid = ormd.getordertid(Long.parseLong(request.getParameter("id")));
                    %>
                    <table>
                        <tr>
                            <td>
                                <b> Họ và tên Khách hàng:</b>
                            </td>
                            <td style="text-transform: uppercase;">
                                <%= orderid.getCustomerFullName() %>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b> Địa chỉ:</b>
                            </td>
                            <td>
                                 <%= orderid.getCustomerAddress()%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b> Số Điện thoại: </b>
                            </td>
                            <td>
                                 <%= orderid.getCustomerPhone() %>
                            </td>
                        </tr>
                            <tr>
                            <td>
                                <b>Email: </b>
                            </td>
                            <td>
                                 <%= orderid.getCustomerEmail()%>
                            </td>
                        </tr>
                        
                    </table>
                </div>
               <!--   Basic Table  -->
               <div class="panel panel-default">
                <div class="panel-heading">
                    Danh sách sản phẩm mua
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    
                                    <th>STT</th>
                                    <th>Tên sản phẩm</th>
                                    <th>Hình ảnh</th>
                                    <th>Giá</th>
                                    <th>Số lượng</th>
                                    <th>Thành Tiền</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <% DecimalFormat formatter = new DecimalFormat("###,###,###"); %>
                                <% int i=1;
                                    float tongtien=0;
                                    ordermodel order = new ordermodel();
                                %>
                                <% for(TbOrderdetail ordt : order.getOrderdetail(Long.parseLong(request.getParameter("id")))){
                                    tongtien = tongtien+ordt.getQuantity()*ordt.getPrice();
                                %>
                                <tr>
                                    <td><% out.print(i); %></td>
                                    <td><%=ordt.getProductName()%></td>
                                    <td><img  src="../assets/img/<%=ordt.getImage()%>" width="100px"></td>
                                    <td><%out.print(formatter.format(ordt.getPrice()));%></td>
                                    <td><%=ordt.getQuantity()%></td>
                                    <td><% out.print(formatter.format(ordt.getPrice()*ordt.getQuantity())); %></td>
                                    
                               </tr>
                                <% i++;}%>
                                <tr>
                                    <td colspan="6"><b>Tổng Tiền : <% out.print(formatter.format(tongtien)); %> VNĐ</b></td>
                                    
                                </tr>
                                
                            </tbody>
                                
                        </table>
                    </div>
                                               
                </div>
            </div>
            <!-- End  Basic Table  -->
             <table>
                                        <tr>
                                            <td>
                                                <a href="../../orderServlet?comand=accept&id=<%= orderid.getIdOrder()%>"><button>Xác nhận</button></a>
                                            </td>
                                            <td style="padding: 10px;">
                                                <a href="../../orderServlet?comand=cancel&id=<%= orderid.getIdOrder()%>"><button>Xóa hóa đơn</button></a>
                                            </td>
                                        </tr>
                                    </table> 
        </div>
    </div>
</div>
<!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->

<div id="footer-sec">
    &copy; 2014 YourCompany | Design By : <a href="http://www.binarytheme.com/" target="_blank">BinaryTheme.com</a>
</div>
<!-- /. FOOTER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="assets/js/bootstrap.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="assets/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="assets/js/custom.js"></script>
</body>
</html>


