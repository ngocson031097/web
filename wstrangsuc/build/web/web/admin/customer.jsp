<%@page import="model.Customermodel"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="model.usermodel" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Quản lý tài khoản người dùng</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!--CUSTOM BASIC STYLES-->
    <link href="assets/css/basic.css" rel="stylesheet" />
    <!--CUSTOM MAIN STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <%@include file="header.jsp" %>
        <!-- /. NAV TOP  -->
        <jsp:include page="nav.jsp"/>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div class="">
               <!--   Basic Table  -->
               <div class="panel panel-default">
                <div class="panel-heading">
                    NGƯỜI DÙNG
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    
                                    <th>STT</th>
                                    <th>Tên người dùng</th>
                                    <th>Họ và tên</th>
                                    <th>email</th>
                                    <th>số điện thoại</th>
                                    <th>giới tính</th>
                                    <th>Xứ lý</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int i=0;
                                
                                    Customermodel cus = new Customermodel();
                                %>
                                <c:forEach var="us" items="<%= cus.getAllCustomer()%>">
                                    <% i++;%>
                                <tr>
                                    <td><%out.print(i);%></td>
                                    <td>${us.username}</td>
                                    <td>${us.fullname}</td>
                                    <td>${us.email}</td>
                                    <td>${us.phoneNumber}</td>
                                    <td>${us.gender}</td>
                                    <td><a href="">Xóa</a></td>
                                </tr>
                                </c:forEach>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- End  Basic Table  -->
        </div>
    </div>
</div>
<!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->

<div id="footer-sec">
    &copy; 2014 YourCompany | Design By : <a href="http://www.binarytheme.com/" target="_blank">BinaryTheme.com</a>
</div>
<!-- /. FOOTER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="assets/js/bootstrap.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="assets/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="assets/js/custom.js"></script>
</body>
</html>


