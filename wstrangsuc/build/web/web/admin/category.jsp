<%@page import="java.util.List"%>
<%@page import="entities.TbCategory"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="model.catmodel"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Quản lý danh mục sản phẩm</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!--CUSTOM BASIC STYLES-->
    <link href="assets/css/basic.css" rel="stylesheet" />
    <!--CUSTOM MAIN STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <%@include file="header.jsp" %>
        <!-- /. NAV TOP  -->
        <jsp:include page="nav.jsp"/>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div class="col-md-9">
                <!-- Add edit category news  -->
                <div class="col-md-8 col-xs-offset-2">    
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <%
                                if(request.getParameter("id")!=null)
                                {
                                    out.print("sửa thông tin danh mục");
                                }
                                else 
                                out.print("thêm danh mục");
                            %>
                        </div>
                        <div class="panel-body">
                           
                            <form method="post" action="../../ManagerCatServlet?command=<%if(request.getParameter("id")!=null) {out.print("update");} else {out.print("insert");}%>">
                                <input type="hidden" name="idcate" value="<%= request.getParameter("id")%>">
                                <!-- rows -->
                                <div class="row" style="margin-top:5px;">
                                    <div class="col-md-4">Tên danh mục</div>
                                    <div class="col-md-8">
                                        <input type="text" value="<%if(request.getParameter("id")!=null){ catmodel cate = new catmodel(); TbCategory cat = cate.getcategoryByid(Integer.parseInt(request.getParameter("id"))); out.print(cat.getCateName());  } %>" name="catename" id="catename" class="form-control" required>
                                    </div>
                                </div>
                                <!-- end rows -->           
                                <!-- rows -->
                                <div class="row" style="margin-top:5px;">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <input type="submit" value="<% if(request.getParameter("command")!=null){ out.print("cập nhật"); } else{out.print("thêm");} %>" class="btn btn-primary">
                                    </div>
                                </div>
                                <!-- end rows -->
                            </form>
                                    
                        </div>
                    </div>
                </div>
                <!-- end Add edit category news -->
                <!--  -->

                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Danh sách danh mục sản phẩm</div>
                        <div class="panel-body">
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <th>STT</th>
                                    <th>Tên danh mục</th>
                                    <th>Thời gian tạo</th>
                                    <th>Thời gian cập nhật</th>
                                    <th style="width:120px;">Quản lý</th>
                                </tr>
                                <%   
                                    int i=1;
                                    catmodel cat = new catmodel();
                                      List<TbCategory> listcate = cat.getAllCategory();
                                    %>
                                    <% for(TbCategory cate: listcate){ %>
                                <tr>
                                    <td><% out.print(i); %></td>
                                    <td><%= cate.getCateName()%></td>
                                    <td><%= cate.getCreatedAt() %></td>
                                    <td><%= cate.getUpdatedAt() %></td>
                                    <td style="text-align:center">
                                        <a href="category.jsp?command=edit&&id=<%= cate.getIdCate()%>">Edit</a>&nbsp;|&nbsp;
                                        <a href="../../ManagerCatServlet?command=delete&&id=<%= cate.getIdCate()%>" onclick="return window.confirm('Bạn có chắc chắn muốn xóa?');">Delete</a>
                                    </td>
                                    
                                </tr>
                                    <%  i++ ;
                                            } %>
                            </table>
                    </div>
                </div>
            </div>
            <!--  -->
        </div>
    </div>
</div>
<!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->

<div id="footer-sec">
    &copy; 2014 YourCompany | Design By : <a href="http://www.binarytheme.com/" target="_blank">BinaryTheme.com</a>
</div>
<!-- /. FOOTER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="assets/js/bootstrap.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="assets/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="assets/js/custom.js"></script>
</body>
</html>


