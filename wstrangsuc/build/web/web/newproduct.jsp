<%-- 
    Document   : newproduct
    Created on : Mar 31, 2018, 1:58:40 PM
    Author     : sonbh
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="model.productmodel" %>
<%@page import="entities.TbProduct" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>



<div class="well well-small">
	<h3>Sản phẩm mới </h3>
	<hr class="soften"/>
	<div class="row-fluid">
		<div id="newProductCar" class="carousel slide">
                    <div class="carousel-inner">
			<div class="item active">
			  <ul class="thumbnails">
                              <% int i=0; %>
                              <% productmodel mdpr = new productmodel(); %>
                              <c:forEach var="pr" items="<%= mdpr.getAllProduct() %>">
                                    
                                    <li class="span3">
                                        <div class="thumbnail">
                                            <a class="zoomTool" href="product_details.jsp?id=${pr.idProduct}" title="add to cart"><span class="icon-search"></span> Xem chi tiết</a>
                                            <a href="#" class="tag"></a>
                                            <a  href="product_details.jsp"><img src="assets/img/${pr.image}" alt=""></a>
                                            
                                            <p>${pr.productName}</p>
                                        </div>
                                    </li>
                                    <%
                                        i++;
                                        if(i%4==0)
                                        {
                                            out.print("</ul></div><div class=\"item\"><ul class=\"thumbnails\">");
                                        }
                                    
                                    %>
                              </c:forEach>
                                   
			  </ul>
                        </div>
                     
                </div>
                                <a class="left carousel-control" href="#newProductCar" data-slide="prev">&lsaquo;</a>
            <a class="right carousel-control" href="#newProductCar" data-slide="next">&rsaquo;</a>
                </div>
</div>
</div>

