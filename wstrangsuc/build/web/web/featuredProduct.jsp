<%-- 
    Document   : featuredProduct
    Created on : Mar 31, 2018, 2:01:53 PM
    Author     : sonbh
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="model.productmodel" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="well well-small">
		  <h3><a class="btn btn-mini pull-right" href="products.jsp" title="View more">VIew More<span class="icon-plus"></span></a>Sản phẩm bán chạy</h3>
		  <hr class="soften"/>
		  <div class="row-fluid">
		  <ul class="thumbnails">
			
			<%
                            productmodel prd = new productmodel();
                            
                        %>
                        <c:forEach var="prrd" items="<%= prd.getProductrandom()%>">
			<li class="span4">
			  <div class="thumbnail">
				<a class="zoomTool" href="product_details.jsp?id=${prrd.idProduct}" title="add to cart"><span class="icon-search"></span> Xem chi tiết</a>
				<a  href="product_details.jsp"><img src="assets/img/${prrd.image}" alt=""/></a>
				<div class="caption">
				  <h5>${prrd.productName}</h5>
				  <h6>
					  
                                      <a class="shopBtn"  href="../CartServlet?comand=addcart&id=${prrd.idProduct}" title="add to cart">Thêm vào giỏ</a>
					  <span class="pull-right">${prrd.price} VND</span>
				  </h6>
				</div>
			  </div>
			</li>
                        </c:forEach>
                  </ul>
                        
	</div>
</div>
