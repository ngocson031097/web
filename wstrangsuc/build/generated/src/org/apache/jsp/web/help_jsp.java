package org.apache.jsp.web;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import entities.Help;
import java.util.ArrayList;
import java.util.List;
import model.helpmodel;

public final class help_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("\t<title>Tư vấn thiết kế</title>\r\n");
      out.write("\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n");
      out.write("    <meta name=\"description\" content=\"\">\r\n");
      out.write("    <meta name=\"author\" content=\"\">\r\n");
      out.write("    <!-- Bootstrap styles -->\r\n");
      out.write("    <link href=\"assets/css/bootstrap.css\" rel=\"stylesheet\"/>\r\n");
      out.write("    <!-- Customize styles -->\r\n");
      out.write("    <link href=\"style.css\" rel=\"stylesheet\"/>\r\n");
      out.write("    <!-- font awesome styles -->\r\n");
      out.write("\t<link href=\"assets/font-awesome/css/font-awesome.css\" rel=\"stylesheet\">\r\n");
      out.write("\r\n");
      out.write("\t\t<!--[if IE 7]>\r\n");
      out.write("\t\t\t<link href=\"css/font-awesome-ie7.min.css\" rel=\"stylesheet\">\r\n");
      out.write("\t\t<![endif]-->\r\n");
      out.write("\r\n");
      out.write("\t\t<!--[if lt IE 9]>\r\n");
      out.write("\t\t\t<script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>\r\n");
      out.write("\t\t<![endif]-->\r\n");
      out.write("\r\n");
      out.write("\t<!-- Favicons -->\r\n");
      out.write("    <link rel=\"shortcut icon\" href=\"assets/ico/favicon.ico\">\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("\t<!-- \r\n");
      out.write("\tUpper Header Section \r\n");
      out.write("-->\r\n");
      out.write("<div class=\"navbar navbar-inverse navbar-fixed-top\">\r\n");
      out.write("\t<div class=\"topNav\">\r\n");
      out.write("\t\t<div class=\"container\">\r\n");
      out.write("\t\t\t<div class=\"alignR\">\r\n");
      out.write("\t\t\t\t<div class=\"pull-left socialNw\">\r\n");
      out.write("\t\t\t\t\t<a href=\"#\"><span class=\"icon-twitter\"></span></a>\r\n");
      out.write("\t\t\t\t\t<a href=\"#\"><span class=\"icon-facebook\"></span></a>\r\n");
      out.write("\t\t\t\t\t<a href=\"#\"><span class=\"icon-youtube\"></span></a>\r\n");
      out.write("\t\t\t\t\t<a href=\"#\"><span class=\"icon-tumblr\"></span></a>\r\n");
      out.write("\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t<a class=\"active\" href=\"index.html\"> <span class=\"icon-home\"></span></a> \r\n");
      out.write("\t\t\t\t<a href=\"#\"><span class=\"icon-user\"></span> Tài khoản</a> \r\n");
      out.write("\t\t\t\t<a href=\"register.html\"><span class=\"icon-edit\"></span> Đăng kí </a> \r\n");
      out.write("\t\t\t\t<a href=\"cart.html\"><span class=\"icon-shopping-cart\"></span> 2 sản phẩm - <span class=\"badge badge-warning\"> 520000Đ</span></a>\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t</div>\r\n");
      out.write("</div>\r\n");
      out.write("<!-- End Upper Header Section -->\r\n");
      out.write("\t<!--\r\n");
      out.write("Lower Header Section \r\n");
      out.write("-->\r\n");
      out.write("<div class=\"container\">\r\n");
      out.write("<div id=\"gototop\"> </div>\r\n");
      out.write("<header id=\"header\">\r\n");
      out.write("<div class=\"row\">\r\n");
      out.write("\t<div class=\"span4\">\r\n");
      out.write("\t<h1>\r\n");
      out.write("\t<a class=\"logo\" href=\"index.html\"><span>Sản phẩm HOT tháng 3</span> \r\n");
      out.write("\t\t<img src=\"assets/img/logo-bootstrap-shoping-cart.png\" alt=\"bootstrap sexy shop\" >\r\n");
      out.write("\t</a>\r\n");
      out.write("\t</h1>\r\n");
      out.write("\t</div>\r\n");
      out.write("\t<div class=\"span8 alignR\">\r\n");
      out.write("\t<p><br> <strong> Hỗ trợ (24/7) :  0800 1234 678 </strong><br><br></p>\r\n");
      out.write("\t<span class=\"btn btn-mini\">[ 2 ] <span class=\"icon-shopping-cart\"></span></span>\r\n");
      out.write("\t<span class=\"btn btn-warning btn-mini\">Đ</span>\r\n");
      out.write("\t<span class=\"btn btn-mini\">$</span>\r\n");
      out.write("\t<span class=\"btn btn-mini\">&euro;</span>\r\n");
      out.write("\t</div>\r\n");
      out.write("</div>\r\n");
      out.write("</header>\r\n");
      out.write("<!-- End Lower Header Section -->\r\n");
      out.write("<!--\r\n");
      out.write("Navigation Bar Section \r\n");
      out.write("-->\r\n");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "nav.jsp", out, false);
      out.write("\r\n");
      out.write("<!-- End Navigation Bar Section -->\r\n");
      out.write("\r\n");
      out.write("<section class=\"container\">\r\n");
      out.write("    <h2>\r\n");
      out.write("        nhập câu hỏi của bạn\r\n");
      out.write("    </h2>\r\n");
      out.write("    <form method=\"post\" action=\"../ManagerhelpServlet?command=search\">\r\n");
      out.write("        <input type=\"text\" name=\"ask\" placeholder=\"nhập câu hỏi bạn cần tư vấn\"/>\r\n");
      out.write("        <input type=\"submit\" value=\"tư vấn\"/> \r\n");
      out.write("    </form>\r\n");
      out.write("    ");

        helpmodel help = new helpmodel();
        List<Help> list = new ArrayList<Help>();
        if(request.getParameter("ask")!=null)
        {
             for(Help h :help.getanshelp(request.getParameter("ask")))
                     {
                         
                     
        
             
        
    
      out.write("\r\n");
      out.write("    <p>");
      out.print( h.getAnswer() );
      out.write("</p>\r\n");
      out.write("    ");
}
      out.write("\r\n");
      out.write("</section>\r\n");
      out.write("<!--\r\n");
      out.write("Footer\r\n");
      out.write("-->\r\n");
      out.write("<footer class=\"footer\">\r\n");
      out.write("<div class=\"row-fluid\">\r\n");
      out.write("<div class=\"span2\">\r\n");
      out.write("<h5>Your Account</h5>\r\n");
      out.write("<a href=\"#\">YOUR ACCOUNT</a><br>\r\n");
      out.write("<a href=\"#\">PERSONAL INFORMATION</a><br>\r\n");
      out.write("<a href=\"#\">ADDRESSES</a><br>\r\n");
      out.write("<a href=\"#\">DISCOUNT</a><br>\r\n");
      out.write("<a href=\"#\">ORDER HISTORY</a><br>\r\n");
      out.write(" </div>\r\n");
      out.write("<div class=\"span2\">\r\n");
      out.write("<h5>Iinformation</h5>\r\n");
      out.write("<a href=\"contact.html\">CONTACT</a><br>\r\n");
      out.write("<a href=\"#\">SITEMAP</a><br>\r\n");
      out.write("<a href=\"#\">LEGAL NOTICE</a><br>\r\n");
      out.write("<a href=\"#\">TERMS AND CONDITIONS</a><br>\r\n");
      out.write("<a href=\"#\">ABOUT US</a><br>\r\n");
      out.write(" </div>\r\n");
      out.write("<div class=\"span2\">\r\n");
      out.write("<h5>Our Offer</h5>\r\n");
      out.write("<a href=\"#\">NEW PRODUCTS</a> <br>\r\n");
      out.write("<a href=\"#\">TOP SELLERS</a><br>\r\n");
      out.write("<a href=\"#\">SPECIALS</a><br>\r\n");
      out.write("<a href=\"#\">MANUFACTURERS</a><br>\r\n");
      out.write("<a href=\"#\">SUPPLIERS</a> <br/>\r\n");
      out.write(" </div>\r\n");
      out.write(" <div class=\"span6\">\r\n");
      out.write("<h5>The standard chunk of Lorem</h5>\r\n");
      out.write("The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for\r\n");
      out.write(" those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et \r\n");
      out.write(" Malorum\" by Cicero are also reproduced in their exact original form, \r\n");
      out.write("accompanied by English versions from the 1914 translation by H. Rackham.\r\n");
      out.write(" </div>\r\n");
      out.write(" </div>\r\n");
      out.write("</footer>\r\n");
      out.write("</div><!-- /container -->\r\n");
      out.write("\r\n");
      out.write("<div class=\"copyright\">\r\n");
      out.write("<div class=\"container\">\r\n");
      out.write("\t<p class=\"pull-right\">\r\n");
      out.write("\t\t<a href=\"#\"><img src=\"assets/img/maestro.png\" alt=\"payment\"></a>\r\n");
      out.write("\t\t<a href=\"#\"><img src=\"assets/img/mc.png\" alt=\"payment\"></a>\r\n");
      out.write("\t\t<a href=\"#\"><img src=\"assets/img/pp.png\" alt=\"payment\"></a>\r\n");
      out.write("\t\t<a href=\"#\"><img src=\"assets/img/visa.png\" alt=\"payment\"></a>\r\n");
      out.write("\t\t<a href=\"#\"><img src=\"assets/img/disc.png\" alt=\"payment\"></a>\r\n");
      out.write("\t</p>\r\n");
      out.write("\t<span>Copyright &copy; 2013<br> bootstrap ecommerce shopping template</span>\r\n");
      out.write("</div>\r\n");
      out.write("</div>\r\n");
      out.write("<a href=\"#\" class=\"gotop\"><i class=\"icon-double-angle-up\"></i></a>\r\n");
      out.write("    <!-- Placed at the end of the document so the pages load faster -->\r\n");
      out.write("    <script src=\"assets/js/jquery.js\"></script>\r\n");
      out.write("\t<script src=\"assets/js/bootstrap.min.js\"></script>\r\n");
      out.write("\t<script src=\"assets/js/jquery.easing-1.3.min.js\"></script>\r\n");
      out.write("    <script src=\"assets/js/jquery.scrollTo-1.4.3.1-min.js\"></script>\r\n");
      out.write("    <script src=\"assets/js/shop.js\"></script>\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
