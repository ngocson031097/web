package org.apache.jsp.web.admin;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.ArrayList;
import java.util.List;
import entities.TbCategory;
import model.productmodel;
import model.catmodel;
import entities.*;

public final class listproduct_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write(" \n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html xmlns=\"http://www.w3.org/1999/xhtml\">\n");
      out.write("<head>\n");
      out.write("    <meta charset=\"utf-8\" />\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n");
      out.write("    <title>Danh sách sản phẩm</title>\n");
      out.write("\n");
      out.write("    <!-- BOOTSTRAP STYLES-->\n");
      out.write("    <link href=\"assets/css/bootstrap.css\" rel=\"stylesheet\" />\n");
      out.write("    <!-- FONTAWESOME STYLES-->\n");
      out.write("    <link href=\"assets/css/font-awesome.css\" rel=\"stylesheet\" />\n");
      out.write("    <!--CUSTOM BASIC STYLES-->\n");
      out.write("    <link href=\"assets/css/basic.css\" rel=\"stylesheet\" />\n");
      out.write("    <!--CUSTOM MAIN STYLES-->\n");
      out.write("    <link href=\"assets/css/custom.css\" rel=\"stylesheet\" />\n");
      out.write("    <!-- GOOGLE FONTS-->\n");
      out.write("    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />\n");
      out.write("    <style>\n");
      out.write("        table tr td {\n");
      out.write("            border: solid 1px black;\n");
      out.write("        }\n");
      out.write("        table th td {\n");
      out.write("            border: solid 1px black;\n");
      out.write("        }\n");
      out.write("                                \n");
      out.write("     </style>\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("    \n");
      out.write("    <div id=\"wrapper\">\n");
      out.write("        <nav class=\"navbar navbar-default navbar-cls-top \" role=\"navigation\" style=\"margin-bottom: 0\">\n");
      out.write("            <div class=\"navbar-header\">\n");
      out.write("                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".sidebar-collapse\">\n");
      out.write("                    <span class=\"sr-only\">Toggle navigation</span>\n");
      out.write("                    <span class=\"icon-bar\"></span>\n");
      out.write("                    <span class=\"icon-bar\"></span>\n");
      out.write("                    <span class=\"icon-bar\"></span>\n");
      out.write("                </button>\n");
      out.write("                <a class=\"navbar-brand\" href=\"index.html\">PNJ</a>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("            <div class=\"header-right\">\n");
      out.write("\n");
      out.write("                <a href=\"message-task.html\" class=\"btn btn-info\" title=\"New Message\"><b>30 </b><i class=\"fa fa-envelope-o fa-2x\"></i></a>\n");
      out.write("                <a href=\"message-task.html\" class=\"btn btn-primary\" title=\"New Task\"><b>40 </b><i class=\"fa fa-bars fa-2x\"></i></a>\n");
      out.write("                <a href=\"login.html\" class=\"btn btn-danger\" title=\"Logout\"><i class=\"fa fa-exclamation-circle fa-2x\"></i></a>\n");
      out.write("\n");
      out.write("            </div>\n");
      out.write("        </nav>\n");
      out.write("        <!-- /. NAV TOP  -->\n");
      out.write("        ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "nav.jsp", out, false);
      out.write("\n");
      out.write("        <!-- /. NAV SIDE  -->\n");
      out.write("        <section>\n");
      out.write("        <div id=\"page-wrapper\">\n");
      out.write("            <div class=\"c\">\n");
      out.write("               <!--   Basic Table  -->\n");
      out.write("               <div class=\"panel panel-default\">\n");
      out.write("                <div class=\"panel-heading\">\n");
      out.write("                   Sản phẩm\n");
      out.write("                   \n");
      out.write("                   <br>\n");
      out.write("                       <a href=\"addproduct.jsp?command=insert\">Thêm mới</a>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"panel-body\">\n");
      out.write("                    <div class=\"table-responsive\">\n");
      out.write("                        \n");
      out.write("                        <table class=\"table\">\n");
      out.write("                            \n");
      out.write("                            <thead>\n");
      out.write("                                <tr>\n");
      out.write("                                    \n");
      out.write("                                    <th>STT</th>\n");
      out.write("                                    <th>Tên sản phẩm</th>\n");
      out.write("                                    <th>giá</th>\n");
      out.write("                                    <th>Số lượng</th>\n");
      out.write("                                    <th>Ảnh</th>\n");
      out.write("                                    <th>danh mục</th>\n");
      out.write("                                    \n");
      out.write("                                    <th>Xử lý</th>\n");
      out.write("                                </tr>\n");
      out.write("                            </thead>\n");
      out.write("                            <tbody>\n");
      out.write("                                ");
 int i=1;
                                
                                    productmodel prdmd = new productmodel();
                                    List<TbProduct> listpro = prdmd.getAllProduct();
                                    
                                    
                                    
                                
      out.write("\n");
      out.write("                                ");
 for (TbProduct product: listpro){ 
      out.write("\n");
      out.write("                                <tr>\n");
      out.write("                                    <td>");
 out.print(i); 
      out.write("</td>\n");
      out.write("                                    <td>");
      out.print( product.getProductName() );
      out.write("</td>\n");
      out.write("                                    <td>");
      out.print( product.getPrice());
      out.write("</td>\n");
      out.write("                                    <td>");
      out.print( product.getQuantity() );
      out.write("</td>\n");
      out.write("                                    <td><img src=\"../assets/img/");
      out.print( product.getImage());
      out.write("\" width=\"100px\" height=\"100px\"></td>\n");
      out.write("                                    <td>\n");
      out.write("                                        \n");
      out.write("                                        ");

                                            catmodel cate = new catmodel();
                                            TbCategory cat = cate.getcategoryByid(product.getIdCate());
                                            
                                        
      out.write("\n");
      out.write("                                        ");
      out.print( cat.getCateName() );
      out.write("\n");
      out.write("                                    </td>\n");
      out.write("                                        \n");
      out.write("                                        <td>\n");
      out.write("                                            <a href=\"updateproduct.jsp?command=edit&&id=");
      out.print( product.getIdProduct());
      out.write("\">Sửa</a><br>\n");
      out.write("                                                <br>\n");
      out.write("                                                    <a href=\"../../ManagerProductServlet?command=delete&&id=");
      out.print( product.getIdProduct() );
      out.write("\" onclick=\"return window.confirm('Bạn có chắc chắn muốn xóa?  ')\">Xóa</a>\n");
      out.write("                                        \n");
      out.write("                                        </td>\n");
      out.write("                                   \n");
      out.write("                                </tr>\n");
      out.write("                                ");
 i++; }
      out.write("\n");
      out.write("                                \n");
      out.write("                                \n");
      out.write("                            </tbody>\n");
      out.write("                        </table>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("            <!-- End  Basic Table  -->\n");
      out.write("        </div>\n");
      out.write("    </div>\n");
      out.write(" </section>\n");
      out.write("                  \n");
      out.write("</div>\n");
      out.write("<!-- /. PAGE WRAPPER  -->\n");
      out.write("</div>\n");
      out.write("<!-- /. WRAPPER  -->\n");
      out.write("<!--<footer>\n");
      out.write("<div id=\"footer-sec\">\n");
      out.write("    &copy; 2014 YourCompany | Design By : <a href=\"http://www.binarytheme.com/\" target=\"_blank\">BinaryTheme.com</a>\n");
      out.write("</div>\n");
      out.write("</footer>-->\n");
      out.write("<!-- /. FOOTER  -->\n");
      out.write("<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->\n");
      out.write("<!-- JQUERY SCRIPTS -->\n");
      out.write("<script src=\"assets/js/jquery-1.10.2.js\"></script>\n");
      out.write("<!-- BOOTSTRAP SCRIPTS -->\n");
      out.write("<script src=\"assets/js/bootstrap.js\"></script>\n");
      out.write("<!-- METISMENU SCRIPTS -->\n");
      out.write("<script src=\"assets/js/jquery.metisMenu.js\"></script>\n");
      out.write("<!-- CUSTOM SCRIPTS -->\n");
      out.write("<script src=\"assets/js/custom.js\"></script>\n");
      out.write("</body><script>\n");
      out.write("    function del()\n");
      out.write("    {\n");
      out.write("        alert(\"Xóa thành công\");\n");
      out.write("    }\n");
      out.write("</script>\n");
      out.write("</html>\n");
      out.write("\n");
      out.write("\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
