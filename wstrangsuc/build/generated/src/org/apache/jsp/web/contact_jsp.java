package org.apache.jsp.web;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class contact_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("    <title>Twitter Bootstrap shopping cart</title>\r\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n");
      out.write("    <meta name=\"description\" content=\"\">\r\n");
      out.write("    <meta name=\"author\" content=\"\">\r\n");
      out.write("    <!-- Bootstrap styles -->\r\n");
      out.write("    <link href=\"assets/css/bootstrap.css\" rel=\"stylesheet\"/>\r\n");
      out.write("    <!-- Customize styles -->\r\n");
      out.write("    <link href=\"style.css\" rel=\"stylesheet\"/>\r\n");
      out.write("    <!-- font awesome styles -->\r\n");
      out.write("\t<link href=\"assets/font-awesome/css/font-awesome.css\" rel=\"stylesheet\">\r\n");
      out.write("\t\t<!--[if IE 7]>\r\n");
      out.write("\t\t\t<link href=\"css/font-awesome-ie7.min.css\" rel=\"stylesheet\">\r\n");
      out.write("\t\t<![endif]-->\r\n");
      out.write("\r\n");
      out.write("\t\t<!--[if lt IE 9]>\r\n");
      out.write("\t\t\t<script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>\r\n");
      out.write("\t\t<![endif]-->\r\n");
      out.write("\r\n");
      out.write("\t<!-- Favicons -->\r\n");
      out.write("    <link rel=\"shortcut icon\" href=\"assets/ico/favicon.ico\">\r\n");
      out.write("  </head>\r\n");
      out.write("<body>\r\n");
      out.write("<!-- \r\n");
      out.write("\tUpper Header Section \r\n");
      out.write("-->\r\n");
      out.write("<div class=\"container\">\r\n");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "header.jsp", out, false);
      out.write("\r\n");
      out.write("<!--\r\n");
      out.write("Navigation Bar Section \r\n");
      out.write("-->\r\n");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "nav.jsp", out, false);
      out.write("\r\n");
      out.write("<!-- \r\n");
      out.write("Body Section \r\n");
      out.write("-->\r\n");
      out.write("\t<hr class=\"soften\">\r\n");
      out.write("\t<div class=\"well well-small\">\r\n");
      out.write("\t<h1>Visit us</h1>\r\n");
      out.write("\t<hr class=\"soften\"/>\t\r\n");
      out.write("\t<div class=\"row-fluid\">\r\n");
      out.write("\t\t<div class=\"span8 relative\">\r\n");
      out.write("\t\t<iframe style=\"width:100%; height:350px\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"http://maps.google.co.uk/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Newbury+Street,+Boston,+MA,+United+States&amp;aq=1&amp;oq=NewBoston,+MA,+United+States&amp;sll=42.347238,-71.084011&amp;sspn=0.014099,0.033023&amp;ie=UTF8&amp;hq=Newbury+Street,+Boston,+MA,+United+States&amp;t=m&amp;ll=42.348994,-71.088248&amp;spn=0.001388,0.006276&amp;z=18&amp;iwloc=A&amp;output=embed\"></iframe>\r\n");
      out.write("\r\n");
      out.write("\t\t<div class=\"absoluteBlk\">\r\n");
      out.write("\t\t<div class=\"well wellsmall\">\r\n");
      out.write("\t\t<h4>Contact Details</h4>\r\n");
      out.write("\t\t<h5>\r\n");
      out.write("\t\t\t2601 Mission St.<br/>\r\n");
      out.write("\t\t\tSan Francisco, CA 94110<br/><br/>\r\n");
      out.write("\t\t\t \r\n");
      out.write("\t\t\tinfo@mysite.com<br/>\r\n");
      out.write("\t\t\t﻿Tel 123-456-6780<br/>\r\n");
      out.write("\t\t\tFax 123-456-5679<br/>\r\n");
      out.write("\t\t\tweb:wwwmysitedomain.com\r\n");
      out.write("\t\t</h5>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t\r\n");
      out.write("\t\t<div class=\"span4\">\r\n");
      out.write("\t\t<h4>Email Us</h4>\r\n");
      out.write("\t\t<form class=\"form-horizontal\">\r\n");
      out.write("        <fieldset>\r\n");
      out.write("          <div class=\"control-group\">\r\n");
      out.write("           \r\n");
      out.write("              <input type=\"text\" placeholder=\"name\" class=\"input-xlarge\"/>\r\n");
      out.write("           \r\n");
      out.write("          </div>\r\n");
      out.write("\t\t   <div class=\"control-group\">\r\n");
      out.write("           \r\n");
      out.write("              <input type=\"text\" placeholder=\"email\" class=\"input-xlarge\"/>\r\n");
      out.write("           \r\n");
      out.write("          </div>\r\n");
      out.write("\t\t   <div class=\"control-group\">\r\n");
      out.write("           \r\n");
      out.write("              <input type=\"text\" placeholder=\"subject\" class=\"input-xlarge\"/>\r\n");
      out.write("          \r\n");
      out.write("          </div>\r\n");
      out.write("          <div class=\"control-group\">\r\n");
      out.write("              <textarea rows=\"3\" id=\"textarea\" class=\"input-xlarge\"></textarea>\r\n");
      out.write("           \r\n");
      out.write("          </div>\r\n");
      out.write("\r\n");
      out.write("            <button class=\"shopBtn\" type=\"submit\">Send email</button>\r\n");
      out.write("\r\n");
      out.write("        </fieldset>\r\n");
      out.write("      </form>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t</div>\r\n");
      out.write("\r\n");
      out.write("\t\r\n");
      out.write("</div>\r\n");
      out.write("<!-- \r\n");
      out.write("Clients \r\n");
      out.write("-->\r\n");
      out.write("<section class=\"our_client\">\r\n");
      out.write("\t<hr class=\"soften\"/>\r\n");
      out.write("\t<h4 class=\"title cntr\"><span class=\"text\">Manufactures</span></h4>\r\n");
      out.write("\t<hr class=\"soften\"/>\r\n");
      out.write("\t<div class=\"row\">\r\n");
      out.write("\t\t<div class=\"span2\">\r\n");
      out.write("\t\t\t<a href=\"#\"><img alt=\"\" src=\"assets/img/1.png\"></a>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t<div class=\"span2\">\r\n");
      out.write("\t\t\t<a href=\"#\"><img alt=\"\" src=\"assets/img/2.png\"></a>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t<div class=\"span2\">\r\n");
      out.write("\t\t\t<a href=\"#\"><img alt=\"\" src=\"assets/img/3.png\"></a>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t<div class=\"span2\">\r\n");
      out.write("\t\t\t<a href=\"#\"><img alt=\"\" src=\"assets/img/4.png\"></a>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t<div class=\"span2\">\r\n");
      out.write("\t\t\t<a href=\"#\"><img alt=\"\" src=\"assets/img/5.png\"></a>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t<div class=\"span2\">\r\n");
      out.write("\t\t\t<a href=\"#\"><img alt=\"\" src=\"assets/img/6.png\"></a>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t</div>\r\n");
      out.write("</section>\r\n");
      out.write("\r\n");
      out.write("<!--\r\n");
      out.write("Footer\r\n");
      out.write("-->\r\n");
      out.write("<footer class=\"footer\">\r\n");
      out.write("<div class=\"row-fluid\">\r\n");
      out.write("<div class=\"span2\">\r\n");
      out.write("<h5>Your Account</h5>\r\n");
      out.write("<a href=\"#\">YOUR ACCOUNT</a><br>\r\n");
      out.write("<a href=\"#\">PERSONAL INFORMATION</a><br>\r\n");
      out.write("<a href=\"#\">ADDRESSES</a><br>\r\n");
      out.write("<a href=\"#\">DISCOUNT</a><br>\r\n");
      out.write("<a href=\"#\">ORDER HISTORY</a><br>\r\n");
      out.write(" </div>\r\n");
      out.write("<div class=\"span2\">\r\n");
      out.write("<h5>Iinformation</h5>\r\n");
      out.write("<a href=\"contact.html\">CONTACT</a><br>\r\n");
      out.write("<a href=\"#\">SITEMAP</a><br>\r\n");
      out.write("<a href=\"#\">LEGAL NOTICE</a><br>\r\n");
      out.write("<a href=\"#\">TERMS AND CONDITIONS</a><br>\r\n");
      out.write("<a href=\"#\">ABOUT US</a><br>\r\n");
      out.write(" </div>\r\n");
      out.write("<div class=\"span2\">\r\n");
      out.write("<h5>Our Offer</h5>\r\n");
      out.write("<a href=\"#\">NEW PRODUCTS</a> <br>\r\n");
      out.write("<a href=\"#\">TOP SELLERS</a><br>\r\n");
      out.write("<a href=\"#\">SPECIALS</a><br>\r\n");
      out.write("<a href=\"#\">MANUFACTURERS</a><br>\r\n");
      out.write("<a href=\"#\">SUPPLIERS</a> <br/>\r\n");
      out.write(" </div>\r\n");
      out.write(" <div class=\"span6\">\r\n");
      out.write("<h5>The standard chunk of Lorem</h5>\r\n");
      out.write("The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for\r\n");
      out.write(" those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et \r\n");
      out.write(" Malorum\" by Cicero are also reproduced in their exact original form, \r\n");
      out.write("accompanied by English versions from the 1914 translation by H. Rackham.\r\n");
      out.write(" </div>\r\n");
      out.write(" </div>\r\n");
      out.write("</footer>\r\n");
      out.write("</div><!-- /container -->\r\n");
      out.write("\r\n");
      out.write("<div class=\"copyright\">\r\n");
      out.write("<div class=\"container\">\r\n");
      out.write("\t<p class=\"pull-right\">\r\n");
      out.write("\t\t<a href=\"#\"><img src=\"assets/img/maestro.png\" alt=\"payment\"></a>\r\n");
      out.write("\t\t<a href=\"#\"><img src=\"assets/img/mc.png\" alt=\"payment\"></a>\r\n");
      out.write("\t\t<a href=\"#\"><img src=\"assets/img/pp.png\" alt=\"payment\"></a>\r\n");
      out.write("\t\t<a href=\"#\"><img src=\"assets/img/visa.png\" alt=\"payment\"></a>\r\n");
      out.write("\t\t<a href=\"#\"><img src=\"assets/img/disc.png\" alt=\"payment\"></a>\r\n");
      out.write("\t</p>\r\n");
      out.write("\t<span>Copyright &copy; 2013<br> bootstrap ecommerce shopping template</span>\r\n");
      out.write("</div>\r\n");
      out.write("</div>\r\n");
      out.write("<a href=\"#\" class=\"gotop\"><i class=\"icon-double-angle-up\"></i></a>\r\n");
      out.write("    <!-- Placed at the end of the document so the pages load faster -->\r\n");
      out.write("    <script src=\"assets/js/jquery.js\"></script>\r\n");
      out.write("\t<script src=\"assets/js/bootstrap.min.js\"></script>\r\n");
      out.write("\t<script src=\"assets/js/jquery.easing-1.3.min.js\"></script>\r\n");
      out.write("    <script src=\"assets/js/jquery.scrollTo-1.4.3.1-min.js\"></script>\r\n");
      out.write("    <script src=\"assets/js/shop.js\"></script>\r\n");
      out.write("  </body>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
