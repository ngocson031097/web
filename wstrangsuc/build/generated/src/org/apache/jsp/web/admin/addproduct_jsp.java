package org.apache.jsp.web.admin;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import entities.TbCategory;
import model.catmodel;
import entities.TbProduct;
import model.productmodel;

public final class addproduct_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(1);
    _jspx_dependants.add("/web/admin/header.jsp");
  }

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_forEach_var_items;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_forEach_var_items = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_forEach_var_items.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n");
      out.write("<head>\r\n");
      out.write("    <meta charset=\"utf-8\" />\r\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\r\n");
      out.write("    <title>Thêm sản phẩm mới</title>\r\n");
      out.write("\r\n");
      out.write("    <!-- BOOTSTRAP STYLES-->\r\n");
      out.write("    <link href=\"assets/css/bootstrap.css\" rel=\"stylesheet\" />\r\n");
      out.write("    <!-- FONTAWESOME STYLES-->\r\n");
      out.write("    <link href=\"assets/css/font-awesome.css\" rel=\"stylesheet\" />\r\n");
      out.write("    <!--CUSTOM BASIC STYLES-->\r\n");
      out.write("    <link href=\"assets/css/basic.css\" rel=\"stylesheet\" />\r\n");
      out.write("    <!--CUSTOM MAIN STYLES-->\r\n");
      out.write("    <link href=\"assets/css/custom.css\" rel=\"stylesheet\" />\r\n");
      out.write("    <!-- GOOGLE FONTS-->\r\n");
      out.write("    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("    <div id=\"wrapper\">\r\n");
      out.write("        ");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<nav class=\"navbar navbar-default navbar-cls-top \" role=\"navigation\" style=\"margin-bottom: 0\">\n");
      out.write("            ");

			String username = null;
				Cookie[] cookies = request.getCookies();
				if(cookies !=null)
				{
				for(Cookie cookie : cookies)
				{
				    if(cookie.getName().equals("username")) 
				    	username = cookie.getValue();
				}
				}
                              
				
			if (username == null) {
                            response.sendRedirect("login.jsp");
                        }
                            
		
      out.write("\n");
      out.write("            <div class=\"navbar-header\">\n");
      out.write("                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".sidebar-collapse\">\n");
      out.write("                    <span class=\"sr-only\">Toggle navigation</span>\n");
      out.write("                    <span class=\"icon-bar\"></span>\n");
      out.write("                    <span class=\"icon-bar\"></span>\n");
      out.write("                    <span class=\"icon-bar\"></span>\n");
      out.write("                </button>\n");
      out.write("                <a class=\"navbar-brand\" href=\"index.jsp\">PNJ</a>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("            <div class=\"header-right\">\n");
      out.write("\n");
      out.write("                <a href=\"message-task.jsp\" class=\"btn btn-info\" title=\"New Message\"><b>30 </b><i class=\"fa fa-envelope-o fa-2x\"></i></a>\n");
      out.write("                <a href=\"message-task.jsp\" class=\"btn btn-primary\" title=\"New Task\"><b>40 </b><i class=\"fa fa-bars fa-2x\"></i></a>\n");
      out.write("                <a href=\"../../LogoutServlet\" class=\"btn btn-danger\" title=\"Logout\"><i class=\"fa fa-exclamation-circle fa-2x\"></i></a>");
      out.print( username);
      out.write("\n");
      out.write("\n");
      out.write("            </div>\n");
      out.write("                \n");
      out.write("        </nav>\n");
      out.write("\r\n");
      out.write("        <!-- /. NAV TOP  -->\r\n");
      out.write("        ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "nav.jsp", out, false);
      out.write("\r\n");
      out.write("        <!-- /. NAV SIDE  -->\r\n");
      out.write("        <div id=\"page-wrapper\">\r\n");
      out.write("            <div class=\"col-md-9\">\r\n");
      out.write("               <!-- card -->\r\n");
      out.write("        <div class=\"card border-primary\">\r\n");
      out.write("            <h2>Thêm sản phẩm</h2>\r\n");
      out.write("            ");

                
                if(request.getParameter("id")!=null)
                {
                    productmodel pr = new productmodel();
                    TbProduct product = new TbProduct();
                    product = pr.getproductid(Integer.parseInt(request.getParameter("id")));
                    System.out.println(request.getParameter("id"));
                }
                
               
      out.write("\r\n");
      out.write("               \r\n");
      out.write("               \r\n");
      out.write("               \r\n");
      out.write("            <div class=\"card-body\">\r\n");
      out.write("                <!-- Nếu muốn upload được file, phải có thuộc tính enctype=\"multipart/form-data\" -->\r\n");
      out.write("                <form method=\"post\" action=\"../../ManagerProductServlet?command=insert\">\r\n");
      out.write("                    <!-- form group -->\r\n");
      out.write("                   \r\n");
      out.write("                    <div class=\"form-group\">\r\n");
      out.write("                        <div class=\"row\">\r\n");
      out.write("                            <div class=\"col-md-2 text-right\">Tên sản phẩm</div>\r\n");
      out.write("                            <div class=\"col-md-10 \">\r\n");
      out.write("                                <input type=\"text\" name=\"tensanpham\"  id=\"\" value=\"\"required=\"không được bỏ trống\" class=\"form-control\" ></div>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("                    <!-- end form group -->\r\n");
      out.write("                    <!-- form group -->\r\n");
      out.write("                    <div class=\"form-group\">\r\n");
      out.write("                        <div class=\"row\">\r\n");
      out.write("                            <div class=\"col-md-2 text-right\">Giá</div>\r\n");
      out.write("                            <div class=\"col-md-10 \">\r\n");
      out.write("                                \r\n");
      out.write("                                <input type=\"number\" name=\"gia\" value=\"\" required class=\"form-control\"></div>\r\n");
      out.write("                        </div>\r\n");
      out.write("                        <br />\r\n");
      out.write("                        <div class=\"form-group\">\r\n");
      out.write("                        <div class=\"row\">\r\n");
      out.write("                            <div class=\"col-md-2 text-right\">Số lượng</div>\r\n");
      out.write("                            <div class=\"col-md-10 \">\r\n");
      out.write("                                \r\n");
      out.write("                                <input type=\"number\" name=\"soluong\" value=\"\" required class=\"form-control\"></div>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div><!-- end form group -->\r\n");
      out.write("                    <div class=\"form-group\">\r\n");
      out.write("                        <div class=\"row\">\r\n");
      out.write("                            <div class=\"col-md-2 text-right\">Ảnh</div>\r\n");
      out.write("                            <div class=\"col-md-10 \">\r\n");
      out.write("                                <input type=\"file\" name=\"anh\" id=\"\">\r\n");
      out.write("                            </div>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("                    <!-- form group -->\r\n");
      out.write("                    \r\n");
      out.write("                    </div>\r\n");
      out.write("                    <!-- end form group -->\r\n");
      out.write("                    <!-- form group -->\r\n");
      out.write("                    <div class=\"form-group\">\r\n");
      out.write("                        <div class=\"row\">\r\n");
      out.write("                            <div class=\"col-md-2 text-right\">từ khóa</div>\r\n");
      out.write("                            <div class=\"col-md-10 \"><input type=\"text\" name=\"tukhoa\" value=\"\" class=\"form-control\" / ></div>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("                    <div class=\"form-group\">\r\n");
      out.write("                        <div class=\"row\">\r\n");
      out.write("                            <div class=\"col-md-2 text-right\">mô tả</div>\r\n");
      out.write("                            <div class=\"col-md-10 \"><textarea name=\"mota\" style=\"width: 490px; border-radius: 3px; height:100px; \"  ></textarea>\r\n");
      out.write("                            </div>\r\n");
      out.write("                        </div> \r\n");
      out.write("                    </div>\r\n");
      out.write("                    <!-- end form group -->\r\n");
      out.write("                    <div class=\"form-group\">\r\n");
      out.write("                        <div class=\"row\">\r\n");
      out.write("                            <div class=\"col-md-2 text-right\">Trạng thái</div>\r\n");
      out.write("                            <div class=\"col-md-10 \">\r\n");
      out.write("                                <input type=\"checkbox\" value=\"\" name=\"trangthai\" />\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("                        <div class=\"form-group\">\r\n");
      out.write("                        <div class=\"row\">\r\n");
      out.write("                            <div class=\"col-md-2 text-right\">danh  mục</div>\r\n");
      out.write("                            <div class=\"col-md-10 \">\r\n");
      out.write("                                \r\n");
      out.write("                                <select name=\"danhmuc\" required>\r\n");
      out.write("                                    <option value=\"0\">--Lựa chọn danh mục--</option>\r\n");
      out.write("                                    ");

                                      catmodel cat = new catmodel();
                                     
                                    
      out.write("\r\n");
      out.write("                                    ");
      //  c:forEach
      org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_0 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
      _jspx_th_c_forEach_0.setPageContext(_jspx_page_context);
      _jspx_th_c_forEach_0.setParent(null);
      _jspx_th_c_forEach_0.setVar("getcat");
      _jspx_th_c_forEach_0.setItems( cat.getAllCategory() );
      int[] _jspx_push_body_count_c_forEach_0 = new int[] { 0 };
      try {
        int _jspx_eval_c_forEach_0 = _jspx_th_c_forEach_0.doStartTag();
        if (_jspx_eval_c_forEach_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
          do {
            out.write("\r\n");
            out.write("                                        <option value=\"");
            out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${getcat.idCate}", java.lang.String.class, (PageContext)_jspx_page_context, null));
            out.write('"');
            out.write('>');
            out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${getcat.cateName}", java.lang.String.class, (PageContext)_jspx_page_context, null));
            out.write("</option>\r\n");
            out.write("                                    ");
            int evalDoAfterBody = _jspx_th_c_forEach_0.doAfterBody();
            if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
              break;
          } while (true);
        }
        if (_jspx_th_c_forEach_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
          return;
        }
      } catch (Throwable _jspx_exception) {
        while (_jspx_push_body_count_c_forEach_0[0]-- > 0)
          out = _jspx_page_context.popBody();
        _jspx_th_c_forEach_0.doCatch(_jspx_exception);
      } finally {
        _jspx_th_c_forEach_0.doFinally();
        _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_0);
      }
      out.write("\r\n");
      out.write("                                </select>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("                    <!-- form group -->\r\n");
      out.write("                    \r\n");
      out.write("                    <!-- end form group -->\r\n");
      out.write("                    <!-- form group -->\r\n");
      out.write("                    \r\n");
      out.write("                    <!-- end form group -->\r\n");
      out.write("                    <!-- form group -->\r\n");
      out.write("                    <div class=\"form-group\">\r\n");
      out.write("                        <div class=\"row\">\r\n");
      out.write("                            <div class=\"col-md-2 text-right\"></div>\r\n");
      out.write("                            <div class=\"col-md-10 \">\r\n");
      out.write("                                <input type=\"submit\" value=\"thêm\" id=\"addnew\"  class=\"btn btn-primary\">\r\n");
      out.write("                                <input type=\"reset\" value=\"Reset\" class=\"btn btn-danger\">\r\n");
      out.write("                            </div>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("                    <!-- end form group -->\r\n");
      out.write("                    \r\n");
      out.write("                </form>\r\n");
      out.write("            </div>\r\n");
      out.write("        </div>\r\n");
      out.write("        </div>\r\n");
      out.write("    </div>\r\n");
      out.write("</div>\r\n");
      out.write("<!-- /. PAGE WRAPPER  -->\r\n");
      out.write("</div>\r\n");
      out.write("<!-- /. WRAPPER  -->\r\n");
      out.write("\r\n");
      out.write("<div id=\"footer-sec\">\r\n");
      out.write("    &copy; 2014 YourCompany | Design By : <a href=\"http://www.binarytheme.com/\" target=\"_blank\">BinaryTheme.com</a>\r\n");
      out.write("</div>\r\n");
      out.write("<!-- /. FOOTER  -->\r\n");
      out.write("<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->\r\n");
      out.write("<!-- JQUERY SCRIPTS -->\r\n");
      out.write("<script src=\"assets/js/jquery-1.10.2.js\"></script>\r\n");
      out.write("<!-- BOOTSTRAP SCRIPTS -->\r\n");
      out.write("<script src=\"assets/js/bootstrap.js\"></script>\r\n");
      out.write("<!-- METISMENU SCRIPTS -->\r\n");
      out.write("<script src=\"assets/js/jquery.metisMenu.js\"></script>\r\n");
      out.write("<!-- CUSTOM SCRIPTS -->\r\n");
      out.write("<script src=\"assets/js/custom.js\"></script>\r\n");
      out.write("</body>\r\n");
      out.write("</html>\r\n");
      out.write("\r\n");
      out.write("\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
