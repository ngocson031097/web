package org.apache.jsp.web;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import model.catmodel;

public final class nav_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_forEach_var_items;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_forEach_var_items = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_forEach_var_items.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<div class=\"navbar\">\n");
      out.write("\t  <div class=\"navbar-inner\">\n");
      out.write("\t\t<div class=\"container\">\n");
      out.write("\t\t  <a data-target=\".nav-collapse\" data-toggle=\"collapse\" class=\"btn btn-navbar\">\n");
      out.write("\t\t\t<span class=\"icon-bar\"></span>\n");
      out.write("\t\t\t<span class=\"icon-bar\"></span>\n");
      out.write("\t\t\t<span class=\"icon-bar\"></span>\n");
      out.write("\t\t  </a>\n");
      out.write("\t\t  <div class=\"nav-collapse\">\n");
      out.write("\t\t\t<ul class=\"nav\">\n");
      out.write("\t\t\t  <li class=\"active\"><a href=\"index.jsp\">Trang chủ</a></li>\n");
      out.write("                          ");
 
                              catmodel prmd = new catmodel();
                              
      out.write("\n");
      out.write("                              ");
      //  c:forEach
      org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_0 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
      _jspx_th_c_forEach_0.setPageContext(_jspx_page_context);
      _jspx_th_c_forEach_0.setParent(null);
      _jspx_th_c_forEach_0.setVar("pr");
      _jspx_th_c_forEach_0.setItems( prmd.getAllCategory() );
      int[] _jspx_push_body_count_c_forEach_0 = new int[] { 0 };
      try {
        int _jspx_eval_c_forEach_0 = _jspx_th_c_forEach_0.doStartTag();
        if (_jspx_eval_c_forEach_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
          do {
            out.write("\n");
            out.write("                              <li >\n");
            out.write("                                  <a href=\"products.jsp?catid=");
            out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pr.idCate}", java.lang.String.class, (PageContext)_jspx_page_context, null));
            out.write('"');
            out.write('>');
            out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pr.cateName}", java.lang.String.class, (PageContext)_jspx_page_context, null));
            out.write("</a>\n");
            out.write("                              </li>\n");
            out.write("                                \n");
            out.write("                              ");
            int evalDoAfterBody = _jspx_th_c_forEach_0.doAfterBody();
            if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
              break;
          } while (true);
        }
        if (_jspx_th_c_forEach_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
          return;
        }
      } catch (Throwable _jspx_exception) {
        while (_jspx_push_body_count_c_forEach_0[0]-- > 0)
          out = _jspx_page_context.popBody();
        _jspx_th_c_forEach_0.doCatch(_jspx_exception);
      } finally {
        _jspx_th_c_forEach_0.doFinally();
        _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_0);
      }
      out.write("\n");
      out.write("    \t\t\t  <li class=\"\"><a href=\"help.jsp\">Tư vấn thiết kế</a></li>\n");
      out.write("\t\t\t  <li class=\"\"><a href=\"contact.jsp\">Liên hệ</a></li>\n");
      out.write("\t\t\t</ul>\n");
      out.write("\t\t\t<form action=\"#\" class=\"navbar-search pull-left\">\n");
      out.write("\t\t\t  <input type=\"text\" placeholder=\"Search\" class=\"search-query span2\">\n");
      out.write("                          \n");
      out.write("\t\t\t</form>\t\n");
      out.write("                              ");

			String username = null;
				Cookie[] cookies = request.getCookies();
				if(cookies !=null)
				{
				for(Cookie cookie : cookies)
				{
				    if(cookie.getName().equals("username")) 
				    	username = cookie.getValue();
				}
				}
                              
				
			if (username == null) {
                            
		
      out.write("\n");
      out.write("\t\t\t<ul class=\"nav pull-right\">\n");
      out.write("\t\t\t<li class=\"dropdown\">\n");
      out.write("\t\t\t\t<a data-toggle=\"dropdown\" class=\"dropdown-toggle\" href=\"#\"><span class=\"icon-lock\"></span> Đăng nhập <b class=\"caret\"></b></a>\n");
      out.write("\t\t\t\t<div class=\"dropdown-menu\">\n");
      out.write("                                    <form class=\"form-horizontal loginFrm\" action=\"../../LoginusServlet\">\n");
      out.write("\t\t\t\t  <div class=\"control-group\">\n");
      out.write("\t\t\t\t\t<input type=\"text\" class=\"span2\" id=\"inputEmail\" placeholder=\"Email\">\n");
      out.write("\t\t\t\t  </div>\n");
      out.write("\t\t\t\t  <div class=\"control-group\">\n");
      out.write("\t\t\t\t\t<input type=\"password\" class=\"span2\" id=\"inputPassword\" placeholder=\"Password\">\n");
      out.write("\t\t\t\t  </div>\n");
      out.write("\t\t\t\t  <div class=\"control-group\">\n");
      out.write("\t\t\t\t\t<label class=\"checkbox\">\n");
      out.write("\t\t\t\t\t<input type=\"checkbox\"> Remember me\n");
      out.write("\t\t\t\t\t</label>\n");
      out.write("\t\t\t\t\t<button type=\"submit\" class=\"shopBtn btn-block\">Đăng nhập</button>\n");
      out.write("\t\t\t\t  </div>\n");
      out.write("\t\t\t\t</form>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t</li>\n");
      out.write("\t\t\t</ul>\n");
      out.write("                        ");
 } 
      out.write("\n");
      out.write("\t\t  </div>\n");
      out.write("\t\t</div>\n");
      out.write("\t  </div>\n");
      out.write("</div>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
