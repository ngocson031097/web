package org.apache.jsp.web.admin;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import entities.TbProduct;
import model.productmodel;

public final class updateproduct_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html xmlns=\"http://www.w3.org/1999/xhtml\">\n");
      out.write("<head>\n");
      out.write("    <meta charset=\"utf-8\" />\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n");
      out.write("    <title>Quản lý sản phẩm</title>\n");
      out.write("\n");
      out.write("    <!-- BOOTSTRAP STYLES-->\n");
      out.write("    <link href=\"assets/css/bootstrap.css\" rel=\"stylesheet\" />\n");
      out.write("    <!-- FONTAWESOME STYLES-->\n");
      out.write("    <link href=\"assets/css/font-awesome.css\" rel=\"stylesheet\" />\n");
      out.write("    <!--CUSTOM BASIC STYLES-->\n");
      out.write("    <link href=\"assets/css/basic.css\" rel=\"stylesheet\" />\n");
      out.write("    <!--CUSTOM MAIN STYLES-->\n");
      out.write("    <link href=\"assets/css/custom.css\" rel=\"stylesheet\" />\n");
      out.write("    <!-- GOOGLE FONTS-->\n");
      out.write("    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("    <div id=\"wrapper\">\n");
      out.write("        <nav class=\"navbar navbar-default navbar-cls-top \" role=\"navigation\" style=\"margin-bottom: 0\">\n");
      out.write("            <div class=\"navbar-header\">\n");
      out.write("                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".sidebar-collapse\">\n");
      out.write("                    <span class=\"sr-only\">Toggle navigation</span>\n");
      out.write("                    <span class=\"icon-bar\"></span>\n");
      out.write("                    <span class=\"icon-bar\"></span>\n");
      out.write("                    <span class=\"icon-bar\"></span>\n");
      out.write("                </button>\n");
      out.write("                <a class=\"navbar-brand\" href=\"index.html\">PNJ</a>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("            <div class=\"header-right\">\n");
      out.write("\n");
      out.write("                <a href=\"message-task.html\" class=\"btn btn-info\" title=\"New Message\"><b>30 </b><i class=\"fa fa-envelope-o fa-2x\"></i></a>\n");
      out.write("                <a href=\"message-task.html\" class=\"btn btn-primary\" title=\"New Task\"><b>40 </b><i class=\"fa fa-bars fa-2x\"></i></a>\n");
      out.write("                <a href=\"login.html\" class=\"btn btn-danger\" title=\"Logout\"><i class=\"fa fa-exclamation-circle fa-2x\"></i></a>\n");
      out.write("\n");
      out.write("            </div>\n");
      out.write("        </nav>\n");
      out.write("        <!-- /. NAV TOP  -->\n");
      out.write("        ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "nav.jsp", out, false);
      out.write("\n");
      out.write("        <!-- /. NAV SIDE  -->\n");
      out.write("        <div id=\"page-wrapper\">\n");
      out.write("            <div class=\"col-md-9\">\n");
      out.write("               <!-- card -->\n");
      out.write("        <div class=\"card border-primary\">\n");
      out.write("            <h2>Thêm sản phẩm</h2>\n");
      out.write("            ");

                
                if(request.getParameter("id")!=null)
                {
                    productmodel pr = new productmodel();
                    TbProduct product = new TbProduct();
                    product = pr.getproductid(Integer.parseInt(request.getParameter("id")));
                }
                
               
      out.write("\n");
      out.write("               \n");
      out.write("            <div class=\"card-body\">\n");
      out.write("                <!-- Nếu muốn upload được file, phải có thuộc tính enctype=\"multipart/form-data\" -->\n");
      out.write("                <form method=\"post\" action=\"../../ManagerProductServlet\">\n");
      out.write("                    <!-- form group -->\n");
      out.write("                    <div>\n");
      out.write("                        <input type=\"text\" name=\"id\" value=\"");
      out.print( request.getParameter("id"));
      out.write("\" />\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"form-group\">\n");
      out.write("                        <div class=\"row\">\n");
      out.write("                            <div class=\"col-md-2 text-right\">Tên sản phẩm</div>\n");
      out.write("                            <div class=\"col-md-10 \">\n");
      out.write("                                <input type=\"text\" name=\"tensanpham\"  id=\"\" value=\"\" ></div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                    <!-- end form group -->\n");
      out.write("                    <!-- form group -->\n");
      out.write("                    <div class=\"form-group\">\n");
      out.write("                        <div class=\"row\">\n");
      out.write("                            <div class=\"col-md-2 text-right\">Giá</div>\n");
      out.write("                            <div class=\"col-md-10 \">\n");
      out.write("                                \n");
      out.write("                                <input type=\"number\" name=\"gia\" value=\"\" required class=\"form-control\"></div>\n");
      out.write("                        </div>\n");
      out.write("\n");
      out.write("                        <div class=\"form-group\">\n");
      out.write("                        <div class=\"row\">\n");
      out.write("                            <div class=\"col-md-2 text-right\">Số lượng</div>\n");
      out.write("                            <div class=\"col-md-10 \">\n");
      out.write("                                \n");
      out.write("                                <input type=\"number\" name=\"soluong\" value=\"\" required class=\"form-control\"></div>\n");
      out.write("                        </div>\n");
      out.write("                    </div><!-- end form group -->\n");
      out.write("                    <div class=\"form-group\">\n");
      out.write("                        <div class=\"row\">\n");
      out.write("                            <div class=\"col-md-2 text-right\">Ảnh</div>\n");
      out.write("                            <div class=\"col-md-10 \">\n");
      out.write("                                <input type=\"file\" name=\"anh\" id=\"\">\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                    <!-- form group -->\n");
      out.write("                    \n");
      out.write("                    </div>\n");
      out.write("                    <!-- end form group -->\n");
      out.write("                    <!-- form group -->\n");
      out.write("                    <div class=\"form-group\">\n");
      out.write("                        <div class=\"row\">\n");
      out.write("                            <div class=\"col-md-2 text-right\">từ khóa</div>\n");
      out.write("                            <div class=\"col-md-10 \"><input type=\"text\" name=\"tukhoa\" value=\"\" /></div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"form-group\">\n");
      out.write("                        <div class=\"row\">\n");
      out.write("                            <div class=\"col-md-2 text-right\">mô tả</div>\n");
      out.write("                            <div class=\"col-md-10 \"><textarea name=\"mota\"></textarea>\n");
      out.write("                                <script type=\"text/javascript\">CKEDITOR.replace('c_description');</script></div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                    <!-- end form group -->\n");
      out.write("                    <div class=\"form-group\">\n");
      out.write("                        <div class=\"row\">\n");
      out.write("                            <div class=\"col-md-2 text-right\">Trạng thái</div>\n");
      out.write("                            <div class=\"col-md-10 \">\n");
      out.write("                               <input type=\"text\" name=\"trangthai\" value=\"\" />\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                        <div class=\"form-group\">\n");
      out.write("                        <div class=\"row\">\n");
      out.write("                            <div class=\"col-md-2 text-right\">danh  mục</div>\n");
      out.write("                            <div class=\"col-md-10 \">\n");
      out.write("                               <input type=\"text\" name=\"danhmuc\" value=\"\" />\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                    <!-- form group -->\n");
      out.write("                    \n");
      out.write("                    <!-- end form group -->\n");
      out.write("                    <!-- form group -->\n");
      out.write("                    \n");
      out.write("                    <!-- end form group -->\n");
      out.write("                    <!-- form group -->\n");
      out.write("                    <div class=\"form-group\">\n");
      out.write("                        <div class=\"row\">\n");
      out.write("                            <div class=\"col-md-2 text-right\"></div>\n");
      out.write("                            <div class=\"col-md-10 \">\n");
      out.write("                                <input type=\"submit\" value=\"thêm\" id=\"addnew\"  class=\"btn btn-primary\">\n");
      out.write("                                <input type=\"reset\" value=\"Reset\" class=\"btn btn-danger\">\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                    <!-- end form group -->\n");
      out.write("                    \n");
      out.write("                </form>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        </div>\n");
      out.write("    </div>\n");
      out.write("</div>\n");
      out.write("<!-- /. PAGE WRAPPER  -->\n");
      out.write("</div>\n");
      out.write("<!-- /. WRAPPER  -->\n");
      out.write("\n");
      out.write("<div id=\"footer-sec\">\n");
      out.write("    &copy; 2014 YourCompany | Design By : <a href=\"http://www.binarytheme.com/\" target=\"_blank\">BinaryTheme.com</a>\n");
      out.write("</div>\n");
      out.write("<!-- /. FOOTER  -->\n");
      out.write("<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->\n");
      out.write("<!-- JQUERY SCRIPTS -->\n");
      out.write("<script src=\"assets/js/jquery-1.10.2.js\"></script>\n");
      out.write("<!-- BOOTSTRAP SCRIPTS -->\n");
      out.write("<script src=\"assets/js/bootstrap.js\"></script>\n");
      out.write("<!-- METISMENU SCRIPTS -->\n");
      out.write("<script src=\"assets/js/jquery.metisMenu.js\"></script>\n");
      out.write("<!-- CUSTOM SCRIPTS -->\n");
      out.write("<script src=\"assets/js/custom.js\"></script>\n");
      out.write("</body>\n");
      out.write("</html>\n");
      out.write("\n");
      out.write("\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
