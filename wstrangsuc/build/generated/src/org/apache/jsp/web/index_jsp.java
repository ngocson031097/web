package org.apache.jsp.web;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import model.catmodel;
import model.productmodel;
import entities.TbProduct;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(2);
    _jspx_dependants.add("/web/nav.jsp");
    _jspx_dependants.add("/web/newproduct.jsp");
  }

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_forEach_var_items;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_forEach_var_items = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_forEach_var_items.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("    <title>Tư vấn và thiết kế trang sức</title>\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n");
      out.write("    <meta name=\"description\" content=\"\">\n");
      out.write("    <meta name=\"author\" content=\"\">\n");
      out.write("    <!-- Bootstrap styles -->\n");
      out.write("    <link href=\"assets/css/bootstrap.css\" rel=\"stylesheet\"/>\n");
      out.write("    <!-- Customize styles -->\n");
      out.write("    <link href=\"style.css\" rel=\"stylesheet\"/>\n");
      out.write("    <!-- font awesome styles -->\n");
      out.write("\t<link href=\"assets/font-awesome/css/font-awesome.css\" rel=\"stylesheet\">\n");
      out.write("\n");
      out.write("\t\t<!--[if IE 7]>\n");
      out.write("\t\t\t<link href=\"css/font-awesome-ie7.min.css\" rel=\"stylesheet\">\n");
      out.write("\t\t<![endif]-->\n");
      out.write("\n");
      out.write("\t\t<!--[if lt IE 9]>\n");
      out.write("\t\t\t<script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>\n");
      out.write("\t\t<![endif]-->\n");
      out.write("\n");
      out.write("\t<!-- Favicons -->\n");
      out.write("    <link rel=\"shortcut icon\" href=\"assets/ico/favicon.ico\">\n");
      out.write("  </head>\n");
      out.write("<body>\n");
      out.write("\t<!--Start of Tawk.to Script-->\n");
      out.write("<!--Start of Zendesk Chat Script-->\n");
      out.write("<script type=\"text/javascript\">\n");
      out.write("window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=\n");
      out.write("d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.\n");
      out.write("_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute(\"charset\",\"utf-8\");\n");
      out.write("$.src=\"https://v2.zopim.com/?5a4M9W4JWnXkDVbjpC5pSrwmQPTe5LlF\";z.t=+new Date;$.\n");
      out.write("type=\"text/javascript\";e.parentNode.insertBefore($,e)})(document,\"script\");\n");
      out.write("</script>\n");
      out.write("<!--End of Zendesk Chat Script-->\n");
      out.write("<!--End of Tawk.to Script-->\n");
      out.write("<!-- \n");
      out.write("\tUpper Header Section \n");
      out.write("-->\n");
      out.write("\n");
      out.write("<div class=\"navbar navbar-inverse navbar-fixed-top\">\n");
      out.write("\t<div class=\"topNav\">\n");
      out.write("\t\t<div class=\"container\">\n");
      out.write("\t\t\t<div class=\"alignR\">\n");
      out.write("\t\t\t\t<div class=\"pull-left socialNw\">\n");
      out.write("\t\t\t\t\t<a href=\"#\"><span class=\"icon-twitter\"></span></a>\n");
      out.write("\t\t\t\t\t<a href=\"#\"><span class=\"icon-facebook\"></span></a>\n");
      out.write("\t\t\t\t\t<a href=\"#\"><span class=\"icon-youtube\"></span></a>\n");
      out.write("\t\t\t\t\t<a href=\"#\"><span class=\"icon-tumblr\"></span></a>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t<a class=\"active\" href=\"index.html\"> <span class=\"icon-home\"></span></a> \n");
      out.write("\t\t\t\t<a href=\"#\"><span class=\"icon-user\"></span> Tài khoản</a> \n");
      out.write("\t\t\t\t<a href=\"register.jsp\"><span class=\"icon-edit\"></span> Đăng kí </a> \n");
      out.write("\t\t\t\t<a href=\"cart.html\"><span class=\"icon-shopping-cart\"></span> 2 sản phẩm - <span class=\"badge badge-warning\"> 520000Đ</span></a>\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t</div>\n");
      out.write("\t</div>\n");
      out.write("</div>\n");
      out.write("<!-- End Upper Header Section -->\n");
      out.write("<!--\n");
      out.write("Lower Header Section \n");
      out.write("-->\n");
      out.write("<div class=\"container\">\n");
      out.write("<div id=\"gototop\"> </div>\n");
      out.write("<header id=\"header\">\n");
      out.write("<div class=\"row\">\n");
      out.write("\t<div class=\"span4\">\n");
      out.write("\t<h1>\n");
      out.write("\t<a class=\"logo\" href=\"index.html\"><span>Sản phẩm HOT tháng 3</span> \n");
      out.write("\t\t<img src=\"assets/img/logo-bootstrap-shoping-cart.png\" alt=\"bootstrap sexy shop\" >\n");
      out.write("\t</a>\n");
      out.write("\t</h1>\n");
      out.write("\t</div>\n");
      out.write("\t<div class=\"span8 alignR\">\n");
      out.write("\t<p><br> <strong> Hỗ trợ (24/7) :  0800 1234 678 </strong><br><br></p>\n");
      out.write("\t<span class=\"btn btn-mini\">[ 2 ] <span class=\"icon-shopping-cart\"></span></span>\n");
      out.write("\t<span class=\"btn btn-warning btn-mini\">Đ</span>\n");
      out.write("\t<span class=\"btn btn-mini\">$</span>\n");
      out.write("\t<span class=\"btn btn-mini\">&euro;</span>\n");
      out.write("\t</div>\n");
      out.write("</div>\n");
      out.write("</header>\n");
      out.write("<!-- End Lower Header Section -->\n");
      out.write("\n");
      out.write("<!--\n");
      out.write("Navigation Bar Section \n");
      out.write("-->\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<div class=\"navbar\">\n");
      out.write("\t  <div class=\"navbar-inner\">\n");
      out.write("\t\t<div class=\"container\">\n");
      out.write("\t\t  <a data-target=\".nav-collapse\" data-toggle=\"collapse\" class=\"btn btn-navbar\">\n");
      out.write("\t\t\t<span class=\"icon-bar\"></span>\n");
      out.write("\t\t\t<span class=\"icon-bar\"></span>\n");
      out.write("\t\t\t<span class=\"icon-bar\"></span>\n");
      out.write("\t\t  </a>\n");
      out.write("\t\t  <div class=\"nav-collapse\">\n");
      out.write("\t\t\t<ul class=\"nav\">\n");
      out.write("\t\t\t  <li class=\"active\"><a href=\"index.jsp\">Trang chủ</a></li>\n");
      out.write("                          ");
 
                              catmodel prmd = new catmodel();
                              
      out.write("\n");
      out.write("                              ");
      //  c:forEach
      org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_0 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
      _jspx_th_c_forEach_0.setPageContext(_jspx_page_context);
      _jspx_th_c_forEach_0.setParent(null);
      _jspx_th_c_forEach_0.setVar("pr");
      _jspx_th_c_forEach_0.setItems( prmd.getAllCategory() );
      int[] _jspx_push_body_count_c_forEach_0 = new int[] { 0 };
      try {
        int _jspx_eval_c_forEach_0 = _jspx_th_c_forEach_0.doStartTag();
        if (_jspx_eval_c_forEach_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
          do {
            out.write("\n");
            out.write("                              <li >\n");
            out.write("                                  <a href=\"products.jsp?catid=");
            out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pr.idCate}", java.lang.String.class, (PageContext)_jspx_page_context, null));
            out.write('"');
            out.write('>');
            out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pr.cateName}", java.lang.String.class, (PageContext)_jspx_page_context, null));
            out.write("</a>\n");
            out.write("                              </li>\n");
            out.write("                                \n");
            out.write("                              ");
            int evalDoAfterBody = _jspx_th_c_forEach_0.doAfterBody();
            if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
              break;
          } while (true);
        }
        if (_jspx_th_c_forEach_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
          return;
        }
      } catch (Throwable _jspx_exception) {
        while (_jspx_push_body_count_c_forEach_0[0]-- > 0)
          out = _jspx_page_context.popBody();
        _jspx_th_c_forEach_0.doCatch(_jspx_exception);
      } finally {
        _jspx_th_c_forEach_0.doFinally();
        _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_0);
      }
      out.write("\n");
      out.write("    \t\t\t  <li class=\"\"><a href=\"help.jsp\">Tư vấn thiết kế</a></li>\n");
      out.write("\t\t\t  <li class=\"\"><a href=\"contact.jsp\">Liên hệ</a></li>\n");
      out.write("\t\t\t</ul>\n");
      out.write("\t\t\t<form action=\"#\" class=\"navbar-search pull-left\">\n");
      out.write("\t\t\t  <input type=\"text\" placeholder=\"Search\" class=\"search-query span2\">\n");
      out.write("                          \n");
      out.write("\t\t\t</form>\t\n");
      out.write("                              ");

			String username = null;
				Cookie[] cookies = request.getCookies();
				if(cookies !=null)
				{
				for(Cookie cookie : cookies)
				{
				    if(cookie.getName().equals("username")) 
				    	username = cookie.getValue();
				}
				}
                              
				
			if (username == null) {
                            
		
      out.write("\n");
      out.write("\t\t\t<ul class=\"nav pull-right\">\n");
      out.write("\t\t\t<li class=\"dropdown\">\n");
      out.write("\t\t\t\t<a data-toggle=\"dropdown\" class=\"dropdown-toggle\" href=\"#\"><span class=\"icon-lock\"></span> Đăng nhập <b class=\"caret\"></b></a>\n");
      out.write("\t\t\t\t<div class=\"dropdown-menu\">\n");
      out.write("                                    <form class=\"form-horizontal loginFrm\" action=\"../../LoginusServlet\">\n");
      out.write("\t\t\t\t  <div class=\"control-group\">\n");
      out.write("\t\t\t\t\t<input type=\"text\" class=\"span2\" id=\"inputEmail\" placeholder=\"Email\">\n");
      out.write("\t\t\t\t  </div>\n");
      out.write("\t\t\t\t  <div class=\"control-group\">\n");
      out.write("\t\t\t\t\t<input type=\"password\" class=\"span2\" id=\"inputPassword\" placeholder=\"Password\">\n");
      out.write("\t\t\t\t  </div>\n");
      out.write("\t\t\t\t  <div class=\"control-group\">\n");
      out.write("\t\t\t\t\t<label class=\"checkbox\">\n");
      out.write("\t\t\t\t\t<input type=\"checkbox\"> Remember me\n");
      out.write("\t\t\t\t\t</label>\n");
      out.write("\t\t\t\t\t<button type=\"submit\" class=\"shopBtn btn-block\">Đăng nhập</button>\n");
      out.write("\t\t\t\t  </div>\n");
      out.write("\t\t\t\t</form>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t</li>\n");
      out.write("\t\t\t</ul>\n");
      out.write("                        ");
 } 
      out.write("\n");
      out.write("\t\t  </div>\n");
      out.write("\t\t</div>\n");
      out.write("\t  </div>\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("<!-- End Navigation Bar Section -->\n");
      out.write("<!-- \n");
      out.write("Body Section \n");
      out.write("-->\n");
      out.write("<div class=\"row\">\n");
      out.write("    ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "bodysection.jsp", out, false);
      out.write("\n");
      out.write("\t<!-- End Body Section -->\n");
      out.write("\t<div class=\"span9\">\n");
      out.write("\t<div class=\"well np\">\n");
      out.write("\t\t<div id=\"myCarousel\" class=\"carousel slide homCar\">\n");
      out.write("            <div class=\"carousel-inner\">\n");
      out.write("\t\t\t  <div class=\"item\">\n");
      out.write("                <img style=\"width:100%\" src=\"assets/img/bootstrap_free-ecommerce.png\" alt=\"bootstrap ecommerce templates\">\n");
      out.write("                <div class=\"carousel-caption\">\n");
      out.write("                      <h4>Sản phẩm hot tháng 3</h4>\n");
      out.write("                      <p><span>Dành cho người phụ nữ yêu thương</span></p>\n");
      out.write("                </div>\n");
      out.write("              </div>\n");
      out.write("\t\t\t  <div class=\"item\">\n");
      out.write("                <img style=\"width:100%\" src=\"assets/img/carousel1.png\" alt=\"bootstrap ecommerce templates\">\n");
      out.write("                <div class=\"carousel-caption\">\n");
      out.write("                      <h4>Sản phẩm hot tháng 3</h4>\n");
      out.write("                      <p><span>Dành cho người phụ nữ yêu thương</span></p>\n");
      out.write("                </div>\n");
      out.write("              </div>\n");
      out.write("\t\t\t  <div class=\"item active\">\n");
      out.write("                <img style=\"width:100%\" src=\"assets/img/carousel3.png\" alt=\"bootstrap ecommerce templates\">\n");
      out.write("                <div class=\"carousel-caption\">\n");
      out.write("                      <h4>Sản phẩm hot tháng 3</h4>\n");
      out.write("                      <p><span>Dành cho người phụ nữ yêu thương</span></p>\n");
      out.write("                </div>\n");
      out.write("              </div>\n");
      out.write("              <div class=\"item\">\n");
      out.write("                <img style=\"width:100%\" src=\"assets/img/bootstrap-templates.png\" alt=\"bootstrap templates\">\n");
      out.write("                <div class=\"carousel-caption\">\n");
      out.write("                      <h4>Sản phẩm hot tháng 3</h4>\n");
      out.write("                      <p><span>Dành cho người phụ nữ yêu thương</span></p>\n");
      out.write("                </div>\n");
      out.write("              </div>\n");
      out.write("            </div>\n");
      out.write("            <a class=\"left carousel-control\" href=\"#myCarousel\" data-slide=\"prev\">&lsaquo;</a>\n");
      out.write("            <a class=\"right carousel-control\" href=\"#myCarousel\" data-slide=\"next\">&rsaquo;</a>\n");
      out.write("          </div>\n");
      out.write("        </div>\n");
      out.write("<!--\n");
      out.write("New Products\n");
      out.write("-->\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<div class=\"well well-small\">\n");
      out.write("\t<h3>Sản phẩm mới </h3>\n");
      out.write("\t<hr class=\"soften\"/>\n");
      out.write("\t<div class=\"row-fluid\">\n");
      out.write("\t\t<div id=\"newProductCar\" class=\"carousel slide\">\n");
      out.write("                    <div class=\"carousel-inner\">\n");
      out.write("\t\t\t<div class=\"item active\">\n");
      out.write("\t\t\t  <ul class=\"thumbnails\">\n");
      out.write("                              ");
 int i=0; 
      out.write("\n");
      out.write("                              ");
 productmodel mdpr = new productmodel(); 
      out.write("\n");
      out.write("                              ");
      //  c:forEach
      org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_1 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
      _jspx_th_c_forEach_1.setPageContext(_jspx_page_context);
      _jspx_th_c_forEach_1.setParent(null);
      _jspx_th_c_forEach_1.setVar("pr");
      _jspx_th_c_forEach_1.setItems( mdpr.getAllProduct() );
      int[] _jspx_push_body_count_c_forEach_1 = new int[] { 0 };
      try {
        int _jspx_eval_c_forEach_1 = _jspx_th_c_forEach_1.doStartTag();
        if (_jspx_eval_c_forEach_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
          do {
            out.write("\n");
            out.write("                                    \n");
            out.write("                                    <li class=\"span3\">\n");
            out.write("                                        <div class=\"thumbnail\">\n");
            out.write("                                            <a class=\"zoomTool\" href=\"product_details.jsp?id=");
            out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pr.idProduct}", java.lang.String.class, (PageContext)_jspx_page_context, null));
            out.write("\" title=\"add to cart\"><span class=\"icon-search\"></span> Xem chi tiết</a>\n");
            out.write("                                            <a href=\"#\" class=\"tag\"></a>\n");
            out.write("                                            <a  href=\"product_details.jsp\"><img src=\"assets/img/");
            out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pr.image}", java.lang.String.class, (PageContext)_jspx_page_context, null));
            out.write("\" alt=\"\"></a>\n");
            out.write("                                            \n");
            out.write("                                            <p>");
            out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pr.productName}", java.lang.String.class, (PageContext)_jspx_page_context, null));
            out.write("</p>\n");
            out.write("                                        </div>\n");
            out.write("                                    </li>\n");
            out.write("                                    ");

                                        i++;
                                        if(i%4==0)
                                        {
                                            out.print("</ul></div><div class=\"item\"><ul class=\"thumbnails\">");
                                        }
                                    
                                    
            out.write("\n");
            out.write("                              ");
            int evalDoAfterBody = _jspx_th_c_forEach_1.doAfterBody();
            if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
              break;
          } while (true);
        }
        if (_jspx_th_c_forEach_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
          return;
        }
      } catch (Throwable _jspx_exception) {
        while (_jspx_push_body_count_c_forEach_1[0]-- > 0)
          out = _jspx_page_context.popBody();
        _jspx_th_c_forEach_1.doCatch(_jspx_exception);
      } finally {
        _jspx_th_c_forEach_1.doFinally();
        _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_1);
      }
      out.write("\n");
      out.write("                                   \n");
      out.write("\t\t\t  </ul>\n");
      out.write("                        </div>\n");
      out.write("                     \n");
      out.write("                </div>\n");
      out.write("                                <a class=\"left carousel-control\" href=\"#newProductCar\" data-slide=\"prev\">&lsaquo;</a>\n");
      out.write("            <a class=\"right carousel-control\" href=\"#newProductCar\" data-slide=\"next\">&rsaquo;</a>\n");
      out.write("                </div>\n");
      out.write("</div>\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("\n");
      out.write("\t<!--\n");
      out.write("\tFeatured Products\n");
      out.write("\t-->\n");
      out.write("        ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "featuredProduct.jsp", out, false);
      out.write("\n");
      out.write("\t\n");
      out.write("\t<div class=\"well well-small\">\n");
      out.write("\t<a class=\"btn btn-mini pull-right\" href=\"#\">View more <span class=\"icon-plus\"></span></a>\n");
      out.write("\tPopular Products \n");
      out.write("\t</div>\n");
      out.write("\t<hr>\n");
      out.write("\t<div class=\"well well-small\">\n");
      out.write("\t<a class=\"btn btn-mini pull-right\" href=\"#\">View more <span class=\"icon-plus\"></span></a>\n");
      out.write("\tBest selling Products \n");
      out.write("\t</div>\n");
      out.write("\t</div>\n");
      out.write("\t</div>\n");
      out.write("<!-- \n");
      out.write("Clients \n");
      out.write("-->\n");
      out.write("<section class=\"our_client\">\n");
      out.write("\t<hr class=\"soften\"/>\n");
      out.write("\t<h4 class=\"title cntr\"><span class=\"text\">Manufactures</span></h4>\n");
      out.write("\t<hr class=\"soften\"/>\n");
      out.write("\t<div class=\"row\">\n");
      out.write("\t\t<div class=\"span2\">\n");
      out.write("\t\t\t<a href=\"#\"><img alt=\"\" src=\"assets/img/1.png\"></a>\n");
      out.write("\t\t</div>\n");
      out.write("\t\t<div class=\"span2\">\n");
      out.write("\t\t\t<a href=\"#\"><img alt=\"\" src=\"assets/img/2.png\"></a>\n");
      out.write("\t\t</div>\n");
      out.write("\t\t<div class=\"span2\">\n");
      out.write("\t\t\t<a href=\"#\"><img alt=\"\" src=\"assets/img/3.png\"></a>\n");
      out.write("\t\t</div>\n");
      out.write("\t\t<div class=\"span2\">\n");
      out.write("\t\t\t<a href=\"#\"><img alt=\"\" src=\"assets/img/4.png\"></a>\n");
      out.write("\t\t</div>\n");
      out.write("\t\t<div class=\"span2\">\n");
      out.write("\t\t\t<a href=\"#\"><img alt=\"\" src=\"assets/img/5.png\"></a>\n");
      out.write("\t\t</div>\n");
      out.write("\t\t<div class=\"span2\">\n");
      out.write("\t\t\t<a href=\"#\"><img alt=\"\" src=\"assets/img/6.png\"></a>\n");
      out.write("\t\t</div>\n");
      out.write("\t</div>\n");
      out.write("</section>\n");
      out.write("\n");
      out.write("<!--\n");
      out.write("Footer\n");
      out.write("-->\n");
      out.write("<footer class=\"footer\">\n");
      out.write("<div class=\"row-fluid\">\n");
      out.write("<div class=\"span2\">\n");
      out.write("<h5>Your Account</h5>\n");
      out.write("<a href=\"#\">YOUR ACCOUNT</a><br>\n");
      out.write("<a href=\"#\">PERSONAL INFORMATION</a><br>\n");
      out.write("<a href=\"#\">ADDRESSES</a><br>\n");
      out.write("<a href=\"#\">DISCOUNT</a><br>\n");
      out.write("<a href=\"#\">ORDER HISTORY</a><br>\n");
      out.write(" </div>\n");
      out.write("<div class=\"span2\">\n");
      out.write("<h5>Iinformation</h5>\n");
      out.write("<a href=\"contact.html\">CONTACT</a><br>\n");
      out.write("<a href=\"#\">SITEMAP</a><br>\n");
      out.write("<a href=\"#\">LEGAL NOTICE</a><br>\n");
      out.write("<a href=\"#\">TERMS AND CONDITIONS</a><br>\n");
      out.write("<a href=\"#\">ABOUT US</a><br>\n");
      out.write(" </div>\n");
      out.write("<div class=\"span2\">\n");
      out.write("<h5>Our Offer</h5>\n");
      out.write("<a href=\"#\">NEW PRODUCTS</a> <br>\n");
      out.write("<a href=\"#\">TOP SELLERS</a><br>\n");
      out.write("<a href=\"#\">SPECIALS</a><br>\n");
      out.write("<a href=\"#\">MANUFACTURERS</a><br>\n");
      out.write("<a href=\"#\">SUPPLIERS</a> <br/>\n");
      out.write(" </div>\n");
      out.write(" <div class=\"span6\">\n");
      out.write("<h5>The standard chunk of Lorem</h5>\n");
      out.write("The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for\n");
      out.write(" those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et \n");
      out.write(" Malorum\" by Cicero are also reproduced in their exact original form, \n");
      out.write("accompanied by English versions from the 1914 translation by H. Rackham.\n");
      out.write(" </div>\n");
      out.write(" </div>\n");
      out.write("</footer>\n");
      out.write("</div><!-- /container -->\n");
      out.write("\n");
      out.write("<div class=\"copyright\">\n");
      out.write("<div class=\"container\">\n");
      out.write("\t<p class=\"pull-right\">\n");
      out.write("\t\t<a href=\"#\"><img src=\"assets/img/maestro.png\" alt=\"payment\"></a>\n");
      out.write("\t\t<a href=\"#\"><img src=\"assets/img/mc.png\" alt=\"payment\"></a>\n");
      out.write("\t\t<a href=\"#\"><img src=\"assets/img/pp.png\" alt=\"payment\"></a>\n");
      out.write("\t\t<a href=\"#\"><img src=\"assets/img/visa.png\" alt=\"payment\"></a>\n");
      out.write("\t\t<a href=\"#\"><img src=\"assets/img/disc.png\" alt=\"payment\"></a>\n");
      out.write("\t</p>\n");
      out.write("\t<span>Copyright &copy; 2013<br> bootstrap ecommerce shopping template</span>\n");
      out.write("</div>\n");
      out.write("</div>\n");
      out.write("<a href=\"#\" class=\"gotop\"><i class=\"icon-double-angle-up\"></i></a>\n");
      out.write("    <!-- Placed at the end of the document so the pages load faster -->\n");
      out.write("    <script src=\"assets/js/jquery.js\"></script>\n");
      out.write("\t<script src=\"assets/js/bootstrap.min.js\"></script>\n");
      out.write("\t<script src=\"assets/js/jquery.easing-1.3.min.js\"></script>\n");
      out.write("    <script src=\"assets/js/jquery.scrollTo-1.4.3.1-min.js\"></script>\n");
      out.write("    <script src=\"assets/js/shop.js\"></script>\n");
      out.write("  </body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
