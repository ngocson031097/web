package org.apache.jsp.web.admin;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.List;
import entities.TbCategory;
import model.catmodel;

public final class category_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n");
      out.write("<head>\r\n");
      out.write("    <meta charset=\"utf-8\" />\r\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\r\n");
      out.write("    <title>Quản lý nhà cung cấp</title>\r\n");
      out.write("\r\n");
      out.write("    <!-- BOOTSTRAP STYLES-->\r\n");
      out.write("    <link href=\"assets/css/bootstrap.css\" rel=\"stylesheet\" />\r\n");
      out.write("    <!-- FONTAWESOME STYLES-->\r\n");
      out.write("    <link href=\"assets/css/font-awesome.css\" rel=\"stylesheet\" />\r\n");
      out.write("    <!--CUSTOM BASIC STYLES-->\r\n");
      out.write("    <link href=\"assets/css/basic.css\" rel=\"stylesheet\" />\r\n");
      out.write("    <!--CUSTOM MAIN STYLES-->\r\n");
      out.write("    <link href=\"assets/css/custom.css\" rel=\"stylesheet\" />\r\n");
      out.write("    <!-- GOOGLE FONTS-->\r\n");
      out.write("    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("    <div id=\"wrapper\">\r\n");
      out.write("        <nav class=\"navbar navbar-default navbar-cls-top \" role=\"navigation\" style=\"margin-bottom: 0\">\r\n");
      out.write("            <div class=\"navbar-header\">\r\n");
      out.write("                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".sidebar-collapse\">\r\n");
      out.write("                    <span class=\"sr-only\">Toggle navigation</span>\r\n");
      out.write("                    <span class=\"icon-bar\"></span>\r\n");
      out.write("                    <span class=\"icon-bar\"></span>\r\n");
      out.write("                    <span class=\"icon-bar\"></span>\r\n");
      out.write("                </button>\r\n");
      out.write("                <a class=\"navbar-brand\" href=\"index.html\">PNJ</a>\r\n");
      out.write("            </div>\r\n");
      out.write("\r\n");
      out.write("            <div class=\"header-right\">\r\n");
      out.write("\r\n");
      out.write("                <a href=\"message-task.html\" class=\"btn btn-info\" title=\"New Message\"><b>30 </b><i class=\"fa fa-envelope-o fa-2x\"></i></a>\r\n");
      out.write("                <a href=\"message-task.html\" class=\"btn btn-primary\" title=\"New Task\"><b>40 </b><i class=\"fa fa-bars fa-2x\"></i></a>\r\n");
      out.write("                <a href=\"login.html\" class=\"btn btn-danger\" title=\"Logout\"><i class=\"fa fa-exclamation-circle fa-2x\"></i></a>\r\n");
      out.write("\r\n");
      out.write("            </div>\r\n");
      out.write("        </nav>\r\n");
      out.write("        <!-- /. NAV TOP  -->\r\n");
      out.write("        ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "nav.jsp", out, false);
      out.write("\r\n");
      out.write("        <!-- /. NAV SIDE  -->\r\n");
      out.write("        <div id=\"page-wrapper\">\r\n");
      out.write("            <div class=\"col-md-9\">\r\n");
      out.write("                <!-- Add edit category news  -->\r\n");
      out.write("                <div class=\"col-md-8 col-xs-offset-2\">    \r\n");
      out.write("                    <div class=\"panel panel-primary\">\r\n");
      out.write("                        <div class=\"panel-heading\">Add edit category news</div>\r\n");
      out.write("                        <div class=\"panel-body\">\r\n");
      out.write("                            <form method=\"post\" action=\"\">\r\n");
      out.write("                                <input type=\"hidden\" name=\"_token\" value=\"{{ csrf_token() }}\">\r\n");
      out.write("                                <!-- rows -->\r\n");
      out.write("                                <div class=\"row\" style=\"margin-top:5px;\">\r\n");
      out.write("                                    <div class=\"col-md-2\">Name</div>\r\n");
      out.write("                                    <div class=\"col-md-10\">\r\n");
      out.write("                                        <input type=\"text\" value=\"\" name=\"c_name\" class=\"form-control\" required>\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <!-- end rows -->           \r\n");
      out.write("                                <!-- rows -->\r\n");
      out.write("                                <div class=\"row\" style=\"margin-top:5px;\">\r\n");
      out.write("                                    <div class=\"col-md-2\"></div>\r\n");
      out.write("                                    <div class=\"col-md-10\">\r\n");
      out.write("                                        <input type=\"submit\" value=\"");
 if(request.getParameter("command")!=""){ out.print("sửa"); } else{out.print("thêm");} 
      out.write("\" class=\"btn btn-primary\">\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <!-- end rows -->\r\n");
      out.write("                            </form>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("                </div>\r\n");
      out.write("                <!-- end Add edit category news -->\r\n");
      out.write("                <!--  -->\r\n");
      out.write("\r\n");
      out.write("                <div class=\"col-md-6 col-xs-offset-3\">\r\n");
      out.write("                    <div class=\"panel panel-primary\">\r\n");
      out.write("                        <div class=\"panel-heading\">Category news</div>\r\n");
      out.write("                        <div class=\"panel-body\">\r\n");
      out.write("                            <table class=\"table table-bordered table-hover\">\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <th>STT</th>\r\n");
      out.write("                                    <th>Tên danh mục</th>\r\n");
      out.write("                                    <th style=\"width:100px;\">Quản lý</th>\r\n");
      out.write("                                </tr>\r\n");
      out.write("                                ");
   
                                    int i=1;
                                    catmodel cat = new catmodel();
                                      List<TbCategory> listcate = cat.getAllCategory();
                                    
      out.write("\r\n");
      out.write("                                    ");
 for(TbCategory cate: listcate){ 
      out.write("\r\n");
      out.write("                                <tr>\r\n");
      out.write("                                    <td>");
 out.print(i); 
      out.write("</td>\r\n");
      out.write("                                    <td>");
      out.print( cate.getCateName());
      out.write("</td>\r\n");
      out.write("                                    <td style=\"text-align:center\">\r\n");
      out.write("                                        <a href=\"#\">Edit</a>&nbsp;|&nbsp;\r\n");
      out.write("                                        <a href=\"#\" onclick=\"return window.confirm('Are you sure?');\">Delete</a>\r\n");
      out.write("                                    </td>\r\n");
      out.write("                                    \r\n");
      out.write("                                </tr>\r\n");
      out.write("                                    ");
  i++ ;
                                            } 
      out.write("\r\n");
      out.write("                            </table>\r\n");
      out.write("                    </div>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("            <!--  -->\r\n");
      out.write("        </div>\r\n");
      out.write("    </div>\r\n");
      out.write("</div>\r\n");
      out.write("<!-- /. PAGE WRAPPER  -->\r\n");
      out.write("</div>\r\n");
      out.write("<!-- /. WRAPPER  -->\r\n");
      out.write("\r\n");
      out.write("<div id=\"footer-sec\">\r\n");
      out.write("    &copy; 2014 YourCompany | Design By : <a href=\"http://www.binarytheme.com/\" target=\"_blank\">BinaryTheme.com</a>\r\n");
      out.write("</div>\r\n");
      out.write("<!-- /. FOOTER  -->\r\n");
      out.write("<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->\r\n");
      out.write("<!-- JQUERY SCRIPTS -->\r\n");
      out.write("<script src=\"assets/js/jquery-1.10.2.js\"></script>\r\n");
      out.write("<!-- BOOTSTRAP SCRIPTS -->\r\n");
      out.write("<script src=\"assets/js/bootstrap.js\"></script>\r\n");
      out.write("<!-- METISMENU SCRIPTS -->\r\n");
      out.write("<script src=\"assets/js/jquery.metisMenu.js\"></script>\r\n");
      out.write("<!-- CUSTOM SCRIPTS -->\r\n");
      out.write("<script src=\"assets/js/custom.js\"></script>\r\n");
      out.write("</body>\r\n");
      out.write("</html>\r\n");
      out.write("\r\n");
      out.write("\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
