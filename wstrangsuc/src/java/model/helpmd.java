/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entities.Help;
import java.util.HashMap;

/**
 *
 * @author sonbh
 */
public class helpmd {
     private HashMap<Integer ,help> helpItem;

    public helpmd() {
        helpItem = new HashMap<>();
    }

    public helpmd(HashMap<Integer, help> helpItem) {
        this.helpItem = helpItem;
    }

    public HashMap<Integer, help> getHelpItem() {
        return helpItem;
    }

    public void setHelpItem(HashMap<Integer, help> helpItem) {
        this.helpItem = helpItem;
    }
    public void Insertstar(Integer key, help h,Integer trb) {
       boolean bln = helpItem.containsKey(key);
       
       if (bln) {
            int n = h.getSolan();
             float tb =(float)(h.getTb()*n/(n+1)) + (float)(trb /(n+1));
               h.setSolan(n + 1);
               h.setTb(tb);
            helpItem.put(h.getHelp().getIdhelp(), h);
       } else {
           h.setSolan(1);
           h.setTb(trb);
            helpItem.put(h.getHelp().getIdhelp(), h);
       }
}
    public void removestar(Integer id) {
        boolean bln = helpItem.containsKey(id);
        if (bln) {
            helpItem.remove(id);
        }
        
}
    public static void main(String[] args) {
        
       float tb = (float)2/3 + (float)1/3;
        System.out.println((float)tb);
    }
    
     
}
