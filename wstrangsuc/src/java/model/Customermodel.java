/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entities.Customer;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author sonbh
 */
public class Customermodel {
    private final SessionFactory sf = HibernateUtil.getSessionFactory();
    private Session session;
    private Transaction trans;
     public List<Customer> getAllCustomer ()
    {
        List<Customer> list = null;
        session = sf.openSession();
        trans = session.beginTransaction();
        try {
            Query query =(Query) session.createQuery("from Customer");
             
            list = (List<Customer>)query.list();
            trans.commit();
            
            
        } catch (Exception e) {
            e.printStackTrace();
    }
        return list;
}
    
     public void addCustomer(Customer cus){
       Session sesionnew = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = sesionnew.beginTransaction();
        sesionnew.save(cus);
        transaction.commit();
      
    }
  public void deleteCate(Customer cus){
       Session sessiondel = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = sessiondel.beginTransaction();
        sessiondel.delete(cus);
        transaction.commit();
        
    }
  public void update_Cate(Customer cus){
       Session sesionupdate = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = sesionupdate.beginTransaction();
        sesionupdate.update(cus);
        transaction.commit();
    
}
}
