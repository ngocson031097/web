/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author sonbh
 */
public class Cart {
    private HashMap<Integer ,Item> cartsItem;

    public Cart() {
        cartsItem = new HashMap<>();
                
    }

    public Cart(HashMap<Integer, Item> cartsItem) {
        this.cartsItem = cartsItem;
    }

    public HashMap<Integer, Item> getCartsItem() {
        return cartsItem;
    }

    public void setCartsItem(HashMap<Integer, Item> cartsItem) {
        this.cartsItem = cartsItem;
    }
    public void insertToCart(Integer key, Item item) {
       boolean bln = cartsItem.containsKey(key);
       if (bln) {
            int quantity_old = item.getQuantity();
            //int i =1;
            item.setQuantity(quantity_old + 1);
            cartsItem.put(item.getProduct().getIdProduct(), item);
       } else {
            cartsItem.put(item.getProduct().getIdProduct(), item);
       }
}
    public void removeToCart(Integer id) {
        boolean bln = cartsItem.containsKey(id);
        if (bln) {
            cartsItem.remove(id);
        }
}
    public int countItem() {
        int count = 0;
        count = cartsItem.size();
        return count;
}
    public double total() {
        int count = 0;
        for (Map.Entry<Integer, Item> list : cartsItem.entrySet()) {
            count += list.getValue().getProduct().getPrice()* list.getValue().getQuantity();
        }
        return count;
}
    
}
