/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entities.Customer;
import entities.TbOrder;
import entities.TbOrderdetail;
import entities.TbProduct;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author sonbh
 */
public class ordermodel {
     private final SessionFactory sf = HibernateUtil.getSessionFactory();
    private Session session;
    private Transaction trans;
    
    public List<TbOrder> getAllOrder()
    {
        List<TbOrder> list = null;
        session = sf.openSession();
        trans = session.beginTransaction();
        try {
            Query query =(Query) session.createQuery("from TbOrder");
             
            list = (List<TbOrder>)query.list();
            trans.commit();
            
            
        } catch (Exception e) {
            e.printStackTrace();
         //   trans.commit();
    }
        return list;
}
    public TbOrder getordertid(long id){
       TbOrder order = null;
       session = sf.openSession();
       trans = session.beginTransaction();
       try{
            Query query = (Query) session.createQuery("from TbOrder where idOrder="+id);
            
         
            order = (TbOrder) query.uniqueResult();
            trans.commit();
//            sf.close();
        }
       catch(HibernateException e){
           e.printStackTrace();
           trans.commit();
           
       }
     
        return order;
}
    
    public List<TbOrderdetail> getOrderdetail(long orderid){
       List<TbOrderdetail> list = null;
       session = sf.openSession();
       trans = session.beginTransaction();
       try{
            Query query = (Query) session.createQuery("from TbOrderdetail where id_order="+orderid);
            
            list = (ArrayList<TbOrderdetail>)query.list();
            trans.commit();
//            sf.close();
        }
       catch(HibernateException e){
           e.printStackTrace();
           //trans.commit();
       }
     
        return list;
            
     
    }
      public void addOrder(TbOrder order){
       Session sesionnew = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = sesionnew.beginTransaction();
        sesionnew.save(order);
        transaction.commit();
      
    }
      public void updateOrder(TbOrder order){
       Session sesionnew = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = sesionnew.beginTransaction();
        sesionnew.update(order);
        transaction.commit();
      
    }
      
      public void addOrderdetail(TbOrderdetail orderdetails){
       Session sesionnew = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = sesionnew.beginTransaction();
        sesionnew.save(orderdetails);
        transaction.commit();
      
    }
  public void DeleteOrder(TbOrder order){
       Session sessiondel = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = sessiondel.beginTransaction();
        sessiondel.delete(order);
        transaction.commit();
        
    }
  public void DeleteOrderdetail(TbOrderdetail orderdetail){
       Session sessiondel = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = sessiondel.beginTransaction();
        sessiondel.delete(orderdetail);
        transaction.commit();
        
    }
  
 
}
