/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;
import org.hibernate.SessionFactory;
import entities.*;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author sonbh
 */
public class usermodel {
    private final SessionFactory sf = HibernateUtil.getSessionFactory();
    private Session session;
    private Transaction trans;
     public List<User> getAllUser()
    {
        List<User> list = null;
        session = sf.openSession();
        trans = session.beginTransaction();
        try {
            Query query =(Query) session.createQuery("from User");
             
            list = (List<User>)query.list();
            trans.commit();
            
            
        } catch (Exception e) {
            e.printStackTrace();
    }
        return list;
     
}
     public User getUserId(int idUser)
     {
         User us = null;
         session = sf.openSession();
         trans = session.beginTransaction();
         try{
             Query query = session.createQuery("from User where id="+idUser);
             us = (User)query.uniqueResult();
             trans.commit();
             
         }
         catch(Exception ex)
         {
             ex.printStackTrace();
         }
         return  us;
     }
     public User getUserName(String username)
     {
         User us = null;
         session = sf.openSession();
         trans = session.beginTransaction();
         try {
             Query query = session.createQuery("from User where username ='"+username+"'");
             us =(User)query.uniqueResult();
             trans.commit();
         } catch (Exception e) {
             e.printStackTrace();
         }
         return us;
         
     }
     public User loginadmin(String username, String userPass) {
         User us = null;
         session = sf.openSession();
         trans = session.beginTransaction();
    try {
        
        Query query = session.createQuery("from User where username = :username and password = :userpass and permission = 1");
        query.setString("username", username);
        query.setString("userpass", userPass);
        us = (User) query.uniqueResult();
        trans.commit();
        return us;
    } catch (Exception e) {
        e.printStackTrace();
         return null;
    }
   
}
    public User login(String user,String pass)
    {
        User cus = null;
         session = sf.openSession();
         trans = session.beginTransaction();
        try{
           Query query = session.createQuery("from User where username = '"+user +"' and password = '"+pass+"'");
            cus = (User) query.uniqueResult();
            trans.commit();
            return cus;
        }
        catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    public void addUser(User us){
       Session sesionnew = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = sesionnew.beginTransaction();
        sesionnew.save(us);
        transaction.commit();
      
    }
  public void deleteuser(User us){
       Session sessiondel = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = sessiondel.beginTransaction();
        sessiondel.delete(us);
        transaction.commit();
        
    }
  public void updateUser(User us){
       Session sesionupdate = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = sesionupdate.beginTransaction();
        sesionupdate.update(us);
        transaction.commit();
    
}
  
}
 