/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;
import org.hibernate.*;
import java.util.*;
import entities.*;
import java.lang.reflect.Array;
import javax.imageio.spi.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
/**
 *
 * 
 * @author sonbh
 */
public class productmodel {
    
    private final SessionFactory sf = HibernateUtil.getSessionFactory();
    private Session session;
    private Transaction trans;
    
    public List<TbProduct> getAllProduct()
    {
        List<TbProduct> list = null;
        session = sf.openSession();
        trans = session.beginTransaction();
        try {
            Query query =(Query) session.createQuery("from TbProduct");
             
            list = (List<TbProduct>)query.list();
            trans.commit();
            
            
        } catch (Exception e) {
            e.printStackTrace();
            trans.commit();
    }
        return list;
}
    
    public List<TbProduct> getProductByIdCat(Integer  madanhmuc){
       List<TbProduct> list = null;
       session = sf.openSession();
       trans = session.beginTransaction();
       try{
            Query query = (Query) session.createQuery("from TbProduct where idCate="+madanhmuc);
            
            list = (ArrayList<TbProduct>)query.list();
            trans.commit();
//            sf.close();
        }
       catch(HibernateException e){
           e.printStackTrace();
           trans.commit();
       }
     
        return list;
            
     
    }
    public List<TbProduct> getProductrandom(){
       List<TbProduct> list = null;
       session = sf.openSession();
       trans = session.beginTransaction();
       try{
            Query query = (Query) session.createQuery("from TbProduct order by rand()");
            query.setMaxResults(3);
            list = (ArrayList<TbProduct>)query.list();
            trans.commit();
//            sf.close();
        }
       catch(HibernateException e){
           e.printStackTrace();
           trans.commit();
       }
     
        return list;        
    
}
    public List<TbProduct> searchProduct(String ten)
    {
       List<TbProduct> list = null;
       session = sf.openSession();
       trans = session.beginTransaction();
       try {
           Query query = (Query) session.createQuery("from TbProduct where productName like'%"+ten+"%'");
           list = (List<TbProduct>)query.list();
           trans.commit();
       }
       catch(HibernateException e){
           e.printStackTrace();
           trans.commit();
       }
       return list;
    }
    public TbProduct getproductid(Integer id){
       TbProduct prid = null;
       session = sf.openSession();
       trans = session.beginTransaction();
       try{
            Query query = (Query) session.createQuery("from TbProduct where idProduct="+id);
            
         
            prid = (TbProduct) query.uniqueResult();
            trans.commit();
//            sf.close();
        }
       catch(HibernateException e){
           e.printStackTrace();
           trans.commit();
           
       }
     
        return prid;
}
 public void addProduct(TbProduct product){
       Session sesionnew = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = sesionnew.beginTransaction();
        sesionnew.save(product);
        transaction.commit();
      
    }
  public void dele_product(TbProduct product){
       Session sessiondel = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = sessiondel.beginTransaction();
        sessiondel.delete(product);
        transaction.commit();
        
    }
  public void update_product(TbProduct product){
       Session sesionupdate = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = sesionupdate.beginTransaction();
        sesionupdate.update(product);
        transaction.commit();
    }
//     public TbProduct loadpro(long idpro) {
//       Session session = HibernateUtil.getSessionFactory().getCurrentSession();
//        Transaction transaction = session.beginTransaction();
//        TbProduct prd = (TbProduct) session.get(TbProduct.class, idpro);
//        transaction.commit();
//        return prd;
//    }
}
    
 
