/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entities.Help;

/**
 *
 * @author sonbh
 */
public class help {
    private Help help;
    private int solan;
    private Float tb;

    public help() {
    }

    public help(Help help, int solan, float tb) {
        this.help = help;
        this.solan = solan;
        this.tb = tb;
    }

    public Help getHelp() {
        return help;
    }

    public void setHelp(Help help) {
        this.help = help;
    }

    public int getSolan() {
        return solan;
    }

    public void setSolan(int solan) {
        this.solan = solan;
    }

    public float getTb() {
        return tb;
    }

    public void setTb(float tb) {
        this.tb = tb;
    }
    
    
    
}
