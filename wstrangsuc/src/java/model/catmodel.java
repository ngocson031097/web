/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;
import org.hibernate.*;
import java.util.*;
import entities.*;
/**
 *
 * @author sonbh
 */
public class catmodel {
     private final SessionFactory sf = HibernateUtil.getSessionFactory();
    private Session session;
    private Transaction trans;
     public List<TbCategory> getAllCategory()
    {
        List<TbCategory> list = null;
        session = sf.openSession();
        trans = session.beginTransaction();
        try {
            Query query =(Query) session.createQuery("from TbCategory");
             
            list = (List<TbCategory>)query.list();
            trans.commit();
            
            
        } catch (Exception e) {
            e.printStackTrace();
            trans.commit();
    }
        return list;
}
     public TbCategory getcategoryByid(Integer id){
       TbCategory prid = null;
       session = sf.openSession();
       trans = session.beginTransaction();
       try{
            Query query = (Query) session.createQuery("from TbCategory where idCate="+id);
            
         
            prid = (TbCategory) query.uniqueResult();
            trans.commit();
//            sf.close();
        }
       catch(HibernateException e){
           e.printStackTrace();
           trans.commit();
           
       }
     
        return prid;
}
    
    

     public void addCategory(TbCategory cate){
       Session sesionnew = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = sesionnew.beginTransaction();
        sesionnew.save(cate);
        transaction.commit();
      
    }
  public void deleteCate(TbCategory cate){
       Session sessiondel = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = sessiondel.beginTransaction();
        sessiondel.delete(cate);
        transaction.commit();
        
    }
  public void update_Cate(TbCategory cate){
       Session sesionupdate = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = sesionupdate.beginTransaction();
        sesionupdate.update(cate);
        transaction.commit();
    }
}
