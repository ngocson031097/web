/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entities.Customer;
import entities.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Customermodel;
import model.usermodel;

/**
 *
 * @author sonbh
 */
public class ManagerUserServlet extends HttpServlet {
    usermodel user = new usermodel();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        User us = user.getUserId(Integer.parseInt(request.getParameter("id")));
        user.deleteuser(us);
        response.sendRedirect("web/admin/user.jsp");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        
        String username = request.getParameter("username");
        String fullname = request.getParameter("fullname");
        String password = request.getParameter("password");
        String email = request.getParameter("email");
        Integer phone = Integer.parseInt(request.getParameter("phoneNumber"));
        Boolean gender = Boolean.parseBoolean(request.getParameter("gender"));
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date datenow = new Date();
        String date = sdfDate.format(datenow);
        String permisson="";
        if(request.getParameter("permission")!=null){
         permisson = request.getParameter("permission");
        }
        else{
            permisson = "0";
        }
        if(request.getParameter("command")!=null)
        {
        switch(request.getParameter("command"))
        {
            case "insert":
            try{
                User us = new User();
                us.setUsername(username);
                us.setFullname(fullname);
                us.setPassword(password);
                us.setEmail(email);
                us.setPhoneNumber(phone);
                us.setGender(gender);
                us.setPermission(permisson);
                us.setCreatedAt(date);
                us.setUpdatedAt(date);
                user.addUser(us);
                response.sendRedirect("web/index.jsp");
                break;
            }
            catch(Exception ex){
                ex.printStackTrace();
            }
            case "update":
                User us = user.getUserId(Integer.parseInt(request.getParameter("id")));
                us.setUsername(username);
                us.setFullname(fullname);
                us.setPassword(password);
                us.setEmail(email);
                us.setPhoneNumber(phone);
                us.setGender(gender);
                us.setPermission(permisson);
                us.setUpdatedAt(date);
                user.updateUser(us);
                response.sendRedirect("web/index.jsp");
                
                
        }
        }
        else if(request.getParameter("act")!=null){
        switch(request.getParameter("act"))
        {
            case "insert":
            
                try{
                
                User us = new User();
                us.setUsername(username);
                us.setFullname(fullname);
                us.setPassword(password);
                us.setEmail(email);
                us.setPhoneNumber(phone);
                us.setGender(gender);
                us.setPermission(permisson);
                us.setCreatedAt(date);
                us.setUpdatedAt(date);
                user.addUser(us);
                response.sendRedirect("web/admin/user.jsp");
               
            }
                catch(Exception ex){
                    
                }
                break;
            
            
            case "update":
            
              try{
                    
                    User person = user.getUserId(Integer.parseInt(request.getParameter("id")));
                    //User us = new User();
                    person.setUsername(username);
                    person.setFullname(fullname);
                    person.setPassword(password);
                    person.setEmail(email);
                    person.setPhoneNumber(phone);
                    person.setGender(gender);
                    person.setPermission(permisson);
                    person.setUpdatedAt(date);
                    user.updateUser(person);
                    response.sendRedirect("web/admin/user.jsp");
                    break;
                }
              catch(Exception ex){
                   ex.printStackTrace();
           }
            
        }
        }
        
       
                
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
