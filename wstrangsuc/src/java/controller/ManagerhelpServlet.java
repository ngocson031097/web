/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entities.Help;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Scanner;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.helpmodel;

/**
 *
 * @author sonbh
 */
public class ManagerhelpServlet extends HttpServlet {
helpmodel help = new helpmodel();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        String cauhoi  = request.getParameter("ask");
        HttpSession session = request.getSession();
         session.setAttribute("cauhoi", cauhoi);
       // HttpSession ssask = request.getSession();
       // ssask.setAttribute("cauhoi", cauhoi);
       //  helpmodel help = new helpmodel();
       
     helpmodel ans = new helpmodel();
   int n = ans.getanshelp().size();
       // String cauhoi="là gì câu hỏi";
        String[] ask = new String[n];
        String[] answer = new String[n];
        int[] id = new int[n];
        int i=0;
       for(Help h:ans.getanshelp())
            { 
                ask[i] = h.getAsk();
                answer[i] = h.getAnswer();
                id[i] = h.getIdhelp();
                i++;
      } 
       int[] a = new int[n];
       int maxx= -1;
        for(int j=0;j<n;j++)
        {
           a[j]=help.sotugiongnhau(cauhoi, ask[j]);
          
        }
        for(int k = 0; k<n;k++)
        {
           if(a[k]>maxx)
           {
               maxx=a[k];
               
           }
         
        }
        for(int y = 0;y<n;y++)
        {
           if(a[y]==maxx && maxx>3)
           {
              
               session.setAttribute("ans", answer[y]);
               session.setAttribute("ask", ask[y]);
               session.setAttribute("id", id[y]);
              
           }
           else if(request.getParameter("ask")==null || maxx <3){
               session.removeAttribute("ans");
           }
          
        }
      
       
        response.sendRedirect("web/help.jsp");
    
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
       HttpSession ss = request.getSession();
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        Help hel = new Help();
        String ask = request.getParameter("cauhoi");
        String as = request.getParameter("cautraloi");
        String command = request.getParameter("comand");
        switch(command)
        {
            case "insert":
            //Help he = new Help();
            
            if(as!="")
            {
                hel.setAsk(ask);
                hel.setAnswer(as);
                help.addhelp(hel);
                
            }
            
            HttpSession session = request.getSession();
            session.invalidate();
            break;
        }
        response.sendRedirect("web/help.jsp");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
