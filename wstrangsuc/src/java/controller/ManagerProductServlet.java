/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entities.TbProduct;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.productmodel;

/**
 *
 * @author sonbh
 */
public class ManagerProductServlet extends HttpServlet {

    
        TbProduct product = new TbProduct();
        productmodel prmd = new productmodel();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
   
     protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
       
    }

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
    
        String url = "";
        
        
       
        TbProduct prdel = prmd.getproductid(Integer.parseInt(request.getParameter("id")));
        prmd.dele_product(prdel);
        
       //url = "/web/admin/listproduct.jsp";
        
        response.sendRedirect("web/admin/listproduct.jsp");
        
        
       
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id;
        String url = "";
        
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String masp = request.getParameter("idProduct");
        String tensanpham = request.getParameter("tensanpham");
        Integer gia = Integer.parseInt(request.getParameter("gia"));
        Integer soluong = Integer.parseInt(request.getParameter("soluong"));
        String anh = request.getParameter("anh");
        String tukhoa = request.getParameter("tukhoa");
        String mota = request.getParameter("mota");
        //Integer trangthai = Integer.parseInt(request.getParameter("trangthai"));
        Integer danhmuc = Integer.parseInt(request.getParameter("danhmuc"));
        switch((request.getParameter("command")))
        {
            case "update" :
                {
                    try{
            TbProduct productupdate =  prmd.getproductid(Integer.parseInt(request.getParameter("idProduct")));
           productupdate.setProductName(tensanpham);
        productupdate.setPrice(gia);
        productupdate.setQuantity(soluong);
        productupdate.setImage(anh);
        productupdate.setKeywords(tukhoa);
        productupdate.setDescription(mota);
        productupdate.setIdCate(danhmuc);
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date datenow = new Date();
        String date = sdfDate.format(datenow);
       // productupdate.setCreatedAt(date.getDate());
       productupdate.setUpdatedAt(date);
        //productupdate.setStatus();
     
           prmd.update_product(productupdate);
           break;
                    }
                    catch(Exception ex)
                    {
                      
                    }
        
        }
            case "insert":
        {
            try{
            TbProduct proinsert = new TbProduct();
           
          proinsert.setProductName(tensanpham);
        proinsert.setPrice(gia);
        proinsert.setQuantity(soluong);
        proinsert.setImage(anh);
        proinsert.setKeywords(tukhoa);
        proinsert.setDescription(mota);
        proinsert.setIdCate(1);
         SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date datenow = new Date();
        String date = sdfDate.format(datenow);
        
        proinsert.setCreatedAt(date);
        proinsert.setUpdatedAt(date);
       
        prmd.addProduct(proinsert);
        break;
            }
            catch(Exception ex){
                
        }
        }
        }
        url = "web/admin/listproduct.jsp";
        response.sendRedirect(url);
//        RequestDispatcher rd = getServletContext().getRequestDispatcher(url);
//        rd.forward(request, response);
        
        
        
        
        
       
    }

 
}
