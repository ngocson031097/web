/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entities.Help;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.help;
import model.helpmd;
import model.helpmodel;

/**
 *
 * @author sonbh
 */
@WebServlet(name = "managerHelp", urlPatterns = {"/managerHelp"})
public class managerHelp extends HttpServlet {
    helpmodel hmd = new helpmodel();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
       //  processRequest(request, response);
        HttpSession session = request.getSession();
        String command = request.getParameter("command");
        Integer id = Integer.parseInt(request.getParameter("id"));
        Integer star = Integer.parseInt(request.getParameter("star"));
        helpmd help = (helpmd) session.getAttribute("sshelp");
        Help gethehelp = hmd.gethelpid(id);
        
        
        
     
        switch(command)
        {
            case "danhgia":
                
                if(help.getHelpItem().containsKey(id)){
                    int solan = help.getHelpItem().get(id).getSolan()+1;
                    float s = (float)((help.getHelpItem().get(id).getTb()*solan)/(solan+1) + star/(solan+1));
                    if(solan>10 && s <2)
                    {
                        
                        hmd.deletehelp(gethehelp);
                        help.removestar(id);
                        response.sendRedirect("web/help.jsp");
                        session.removeAttribute("ans");
                        session.removeAttribute("cauhoi");
                        
                    }
                    else{ 
                    help.Insertstar(id, new help(gethehelp,help.getHelpItem().get(id).getSolan(),star),star);
                    response.sendRedirect("web/help.jsp");
                    session.removeAttribute("ans");
                    session.removeAttribute("cauhoi");
                   
                    }
                }
                else
                {
                    help.Insertstar(id, new help(gethehelp,1,star),star);
                    response.sendRedirect("web/help.jsp");
                    session.removeAttribute("ans");
                    session.removeAttribute("cauhoi");
                }
               break;
        }
        session.setAttribute("sshelp", help);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
