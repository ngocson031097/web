/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entities.TbCategory;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.catmodel;

/**
 *
 * @author sonbh
 */
public class ManagerCatServlet extends HttpServlet {

        catmodel cate = new catmodel();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        response.setContentType("text/html;charset=UTF-8");
//        try (PrintWriter out = response.getWriter()) {
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet ManagerCatServlet</title>");            
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Servlet ManagerCatServlet at " + request.getContextPath() + "</h1>");
//            out.println("</body>");
//            out.println("</html>");
//        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        
        // processRequest(request, response);
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        
        if(request.getParameter("id")!=null)
        {
            TbCategory catedele = cate.getcategoryByid(Integer.parseInt(request.getParameter("id")));
            
            cate.deleteCate(catedele);
            String url = "web/admin/category.jsp";
        response.sendRedirect(url);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        
        String cateName = request.getParameter("catename");
        
        switch(request.getParameter("command"))
        {
            case "insert":
            {
                
                TbCategory catinsert = new TbCategory();
                
                catinsert.setCateName(cateName);
//                catinsert.setCreatedAt(1);
//                catinsert.setKeywords("từ khóa");
//                catinsert.setDescription("mota");
//                catinsert.setOrder(1);

               
                SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date datenow = new Date();
        String date = sdfDate.format(datenow);
               catinsert.setUpdatedAt(date);
              catinsert.setCreatedAt(date);
               
                cate.addCategory(catinsert);
                break;
            }
            case "update":
            {
                
                TbCategory catupdate = cate.getcategoryByid(Integer.parseInt(request.getParameter("idcate")));
                catupdate.setCateName(cateName);
                SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date datenow = new Date();
        String date = sdfDate.format(datenow);
                
                //catupdate.setCreatedAt(date.getDate());
                catupdate.setUpdatedAt(date);
                
                cate.update_Cate(catupdate);
                
                break;
                
            }
        }
        String url = "web/admin/category.jsp";
        response.sendRedirect(url);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
