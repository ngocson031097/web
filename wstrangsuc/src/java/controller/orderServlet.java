/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entities.TbOrder;
import entities.TbOrderdetail;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Cart;
import model.Item;
import model.Sendmail;
import model.ordermodel;
import model.usermodel;
import org.hibernate.Session;

/**
 *
 * @author sonbh
 */
@WebServlet(name = "orderServlet", urlPatterns = {"/orderServlet"})
public class orderServlet extends HttpServlet {
        ordermodel ordermd = new ordermodel();
        DecimalFormat formatter = new DecimalFormat("###,###,###");
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        TbOrder orderac = ordermd.getordertid(Long.parseLong(request.getParameter("id")));
        String fullname = orderac.getCustomerFullName();
        String address = orderac.getCustomerAddress();
        String email = orderac.getCustomerEmail();
        String phone = orderac.getCustomerPhone();
        String totalmoney = formatter.format(orderac.getTotalMoney());
        //int status = 
        
        String create = orderac.getCreatedAt();
        Date date = new Date();
        String nd ="<table style=\"border:1px black solid\" cellspacing=\"0\">\n" +
"		<tr>\n" +
"			<th style=\"border:1px black solid\" >Tên sản phẩm</th>\n" +
"			<th style=\"border:1px black solid\" >Giá</th>\n" +
"			<th style=\"border:1px black solid\" >Số lượng</th>\n" +
"			<th style=\"border:1px black solid\" >Thành tiền</th>\n" +
"		</tr>";
        for(TbOrderdetail product : ordermd.getOrderdetail(Long.parseLong(request.getParameter("id")))){
            nd +="<tr style=\"border:1px black solid\"><td style=\"border:1px black solid\">"+ product.getProductName()+"</td><td style=\"border:1px black solid\">"+formatter.format(product.getPrice())+"</td><td style=\"border:1px black solid\">" + product.getQuantity() + "</td><td style=\"border:1px black solid\">" + formatter.format(product.getPrice()*product.getQuantity()) +"</td></tr>";
            
            
        }
        nd+="</table><b>Tổng tiền là:</b>"+totalmoney+"VNĐ";
        switch(request.getParameter("comand"))
        {
            case "accept":
                orderac.setStatus(1);
                ordermd.updateOrder(orderac);
                Sendmail mail = new Sendmail();
                Sendmail.sendMail(orderac.getCustomerEmail(), "Xác nhận đặt hàng", nd);
                Sendmail.sendMail("ngocson031097@gmail.com", "San pham da ban cho " + orderac.getCustomerFullName(), nd);
                 response.sendRedirect("web/admin/order.jsp");
                break;
            case "cancel":
                ordermd.DeleteOrder(orderac);
                for(TbOrderdetail product : ordermd.getOrderdetail(Long.parseLong(request.getParameter("id")))){
                    ordermd.DeleteOrderdetail(product);
                }
                response.sendRedirect("web/admin/order.jsp");
                break;
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
         request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        
       // String username = request.getParameter("username");
        String fullname = request.getParameter("fullname");
        String address = request.getParameter("address");
        String email = request.getParameter("email");
        String phone = request.getParameter("phoneNumber");
      //  Boolean gender = Boolean.parseBoolean(request.getParameter("gender"));
      HttpSession session = request.getSession();
                Cart cart = (Cart) session.getAttribute("cart");
                float tong = (float)cart.total();
                SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date datenow = new Date();
        String date = sdfDate.format(datenow);
        
        switch(request.getParameter("command"))
        {
            case "insert":
            {
               long idOrder = (long) new Date().getTime();
                TbOrder order  = new TbOrder();
                order.setIdOrder(idOrder);
                order.setCustomerFullName(fullname);
                order.setCustomerAddress(address);
                order.setCustomerPhone(phone);
                order.setCustomerEmail(email);
               
                order.setCreatedAt(date);
                order.setUpdateAt(date);
                order.setTotalMoney(tong);
                ordermd.addOrder(order);
                for (Map.Entry<Integer, Item> list : cart.getCartsItem().entrySet()) {
                    TbOrderdetail orderdetails = new TbOrderdetail();
                   
                    orderdetails.setProductName(list.getValue().getProduct().getProductName());
                    orderdetails.setImage(list.getValue().getProduct().getImage());
                    orderdetails.setPrice(list.getValue().getProduct().getPrice());
                    orderdetails.setQuantity(list.getValue().getQuantity());
                    orderdetails.setTotalMoney((float)list.getValue().getProduct().getPrice()*list.getValue().getProduct().getQuantity());
                    orderdetails.setIdOrder(String.valueOf(idOrder));
                    orderdetails.setCreateAt(date);
                    orderdetails.setUpdateAt(date);
                   ordermd.addOrderdetail(orderdetails);
                    
                    
                }
                session.removeAttribute("cart");
                
              
                response.sendRedirect("web/index.jsp");
                break;
            }
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
